/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        extend: {
            keyframes: {
                bounce: {
                    '0%, 100%': { transform: 'translateY(-15px)' },
                    '50%': { transform: 'translateY(0px)' },
                },
            },
            animation: {
                bounce: 'bounce 3s linear infinite',
            },
        },

        screens: {
            xl: '1280px',
            lg: { max: '1280px' },
            md: { max: '991px' },
            sm: { max: '767px' },
            xs: { max: '575px' },
        },
    },
    plugins: [],
};
