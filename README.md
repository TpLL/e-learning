# Link: https://e-learning-bay.vercel.app

* Tài khoản dùng thử: user123
* Mật khẩu dùng thử: User0101@ (chữ U viết hoa)

# Các chức năng chính:
## Trang người dùng ('/')  
    +Tìm kiếm khóa học 
    +Đăng ký khóa học 
    +Xóa khóa học đã đăng ký 
    +Chỉnh sửa thông tin người dùng 
    +Responsive 

## Trang admin ('/admin/quanlynguoidung')
    +Link trang: https://e-learning-bay.vercel.app/admin/quanlynguoidung  
    +CRUD người dùng + khóa học. 
    +Responsive 

## Phân công:
    Phi Long: trang người dùng '/' 
    Tuấn Kiệt: trang admin '/admin/quanlynguoidung'