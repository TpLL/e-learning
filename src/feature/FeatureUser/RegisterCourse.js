import React, { useEffect } from "react";
import { Modal, Table } from "antd";
import { useState } from "react";
import { Button, Form, Select } from "antd";
import { userAPI } from "../../services/QuanLyAPI/severAPI";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteCourseUserThunk,
  registerCourseThunk,
} from "../../app/elearningReducer";

export default function RegisterCourse(props) {
  // MODAL ANTD
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // SELECT & INPUT ANTD
  const listUser = useSelector((state) => state.elearnningReducer.listUser);
  const [listCourseUser, setListCourseUser] = useState();
  const [listCourseWait, setListCourseWait] = useState();
  const [listCourseDone, setListCourseDone] = useState();
  const dispatch = useDispatch();
  const registerCourse = useSelector(
    (state) => state.elearnningReducer.registerCourse
  );
  console.log(props);

  useEffect(() => {
    userAPI
      .getListCourseWait({ taiKhoan: `${props.user.taiKhoan}` })
      .then((res) => {
        setListCourseWait(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
    userAPI
      .getListCourseUser(props.user.taiKhoan)
      .then((res) => {
        setListCourseUser(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [listUser]);

  useEffect(() => {
    userAPI
      .getListCourseDone({ taiKhoan: `${props.user.taiKhoan}` })
      .then((res) => {
        setListCourseDone(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [listUser, registerCourse]);

  const renderListCourseUser = () => {
    return listCourseUser?.map((course, index) => {
      return (
        <Select.Option value={course.maKhoaHoc} key={index}>
          {course.tenKhoaHoc}
        </Select.Option>
      );
    });
  };

  const renderListCourseWait = () => {
    return listCourseWait?.map((course, index) => {
      return {
        stt: index + 1,
        tenKhoaHoc: course.tenKhoaHoc,
      };
    });
  };

  const handleDeleteCourseDone = (maKhoaHoc, taiKhoan) => {
    return dispatch(
      deleteCourseUserThunk({
        maKhoaHoc: `${maKhoaHoc}`,
        taiKhoan: `${taiKhoan}`,
      })
    );
  };

  const renderListCourseDone = () => {
    return listCourseDone?.map((course, index) => {
      return {
        stt: index + 1,
        tenKhoaHoc: course.tenKhoaHoc,
        choXacNhan: (
          <button
            className="border rounded bg-green-700 p-1 text-white"
            id={course.maKhoaHoc}
            onClick={() => {
              handleDeleteCourseDone(course.maKhoaHoc, props.user.taiKhoan);
            }}
          >
            Huỷ ghi danh
          </button>
        ),
      };
    });
  };

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const onFinish = (values) => {
    dispatch(
      registerCourseThunk({
        maKhoaHoc: values.maKhoaHoc,
        taiKhoan: `${props.user.taiKhoan}`,
      })
    );
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  // TABLE ANTD
  const columns = [
    {
      title: "STT",
      dataIndex: "stt",
      align: "center",
    },
    {
      title: "Tên khoá học",
      dataIndex: "tenKhoaHoc",
      align: "center",
    },
    {
      title: "Chờ xác nhận",
      dataIndex: "choXacNhan",
      align: "center",
    },
  ];

  // DATA WAITING COURSE
  const dataWait = renderListCourseWait();

  // DATA DONE COURSE

  const dataDone = renderListCourseDone();
  return (
    <div>
      <Button
        className="border bg-green-500 h-10 w-22"
        type=""
        onClick={showModal}
      >
        GHI DANH
      </Button>
      <Modal
        footer=""
        title=""
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        width={800}
      >
        <div className="">
          <Form
            name="nest-messages"
            onFinish={onFinish}
            className="flex space-x-2 border-b-2"
            size="large"
            style={{
              maxWidth: 700,
            }}
            // validateMessages={validateMessages}
          >
            <h1 className=" text-xl font-bold mb-3 text-white">Chọn khóa học</h1>

            <Form.Item
              name="maKhoaHoc"
              style={{ width: 400 }}
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập Mã nhóm!",
                },
              ]}
            >
              <Select className="" placeholder="Mã nhóm">
                {renderListCourseUser()}
              </Select>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                ...layout.wrapperCol,
                offset: 6,
              }}
            >
              <Button type="primary" htmlType="submit" className="buttonLogin">
                GHI DANH
              </Button>
            </Form.Item>
          </Form>
        </div>
        <div className="text-white">
          <p>KHOÁ HỌC CHỜ XÁC THỰC</p>
          <Table columns={columns} dataSource={dataWait} size="middle" />
        </div>
        <div className="text-white">
          <p>KHOÁ HỌC ĐÃ GHI DANH</p>
          <Table columns={columns} dataSource={dataDone} size="middle" />
        </div>
      </Modal>
    </div>
  );
}

/////////
