import React from 'react';
import { Button, Form, Input, Select } from 'antd';
import { addUserThunk } from '../../app/elearningReducer';
import { useDispatch } from 'react-redux';

export default function AddUserFormAntd() {
    const dispatch = useDispatch();
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const onFinish = (values) => {
        console.log(values);
        dispatch(addUserThunk(values));
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div>
            <Form
                name="nest-messages"
                onFinish={onFinish}
                style={{
                    maxWidth: 500,
                }}
                // validateMessages={validateMessages}
            >
                <h1
                    className="text-center text-3xl font-bold mb-3"
                    style={{ color: 'red' }}
                >
                    THÊM NGƯỜI DÙNG
                </h1>
                <Form.Item
                    error="Tài khoản"
                    name="taiKhoan"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập Tài khoản!',
                        },
                    ]}
                >
                    <Input
                        placeholder="Tài khoản"
                        className=""
                        // style={{ padding: 15, backgroundColor: "rgb(196, 192, 192)" }}
                    />
                </Form.Item>
                <Form.Item
                    name="hoTen"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập Họ tên!',
                        },
                    ]}
                >
                    <Input placeholder="Họ tên" className="" />
                </Form.Item>
                <Form.Item
                    name="matKhau"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập Mật khẩu!',
                        },
                    ]}
                >
                    <Input
                        type="password"
                        placeholder="Mật khẩu"
                        className=""
                    />
                </Form.Item>
                <Form.Item
                    name="email"
                    rules={[
                        {
                            type: 'email',
                            required: true,
                            message: 'Vui lòng nhập Email!',
                        },
                    ]}
                >
                    <Input placeholder="Email" className="" />
                </Form.Item>
                <Form.Item
                    name="soDT"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập Số điện thoại!',
                        },
                    ]}
                >
                    <Input placeholder="Số điện thoại" className="" />
                </Form.Item>
                <Form.Item
                    name="maLoaiNguoiDung"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập Loại người dùng!',
                        },
                    ]}
                >
                    <Select className="" placeholder="Loại người dùng">
                        <Select.Option value="GV">Giáo vụ</Select.Option>
                        <Select.Option value="HV">Học viên</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="maNhom"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập Mã nhóm!',
                        },
                    ]}
                >
                    <Select className="" placeholder="Mã nhóm">
                        <Select.Option value="GP01">GP01</Select.Option>
                        <Select.Option value="GP02">GP02</Select.Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        ...layout.wrapperCol,
                        offset: 6,
                    }}
                >
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="buttonLogin"
                    >
                        Thêm người dùng
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}
