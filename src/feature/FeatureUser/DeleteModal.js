import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteUserThunk } from "../../app/elearningReducer";

export default function DeleteModal(props) {
  const dispatch = useDispatch();
  // MODAL DELETE
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState(
    "Bạn có muốn xoá tài khoản này không ?"
  );
  const showModal = () => {
    setOpen(true);
  };
  const handleOk = () => {
    dispatch(deleteUserThunk(props.user));
    setModalText("Thao tác đang được thực hiện!");
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 1000);
  };
  const handleCancel = () => {
    console.log("Clicked cancel button");
    setOpen(false);
  };
  return (
    <div className="">
      <Button type="" onClick={showModal} className="w-14 h-10">
        <span className="text-center">Xoá</span>
      </Button>
      <Modal
        title="Title"
        style={{color:"white"}}
        open={open}
        onOk={handleOk}
        okType="bg-none border-white text-white"
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <p>{modalText}</p>
      </Modal>
    </div>
  );
}
