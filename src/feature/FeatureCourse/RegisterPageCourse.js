import React, { useEffect } from 'react';
import { Input, Modal, Space, Table } from 'antd';
import { useState } from 'react';
import { Button, Form, Select } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { courseAPI, userAPI } from '../../services/QuanLyAPI/severAPI';
import {
    deleteCourseUserThunk,
    registerCourseThunk,
} from '../../app/elearningReducer.jsx';
export default function RegisterPageCourse(props) {
    // MODAL ANTD
    const [isModalOpen, setIsModalOpen] = useState(false);
    const showModal = () => {
        setIsModalOpen(true);
    };
    const handleOk = () => {
        setIsModalOpen(false);
    };
    const handleCancel = () => {
        setIsModalOpen(false);
    };

    // SELECT & INPUT ANTD

    const [listUserUnregister, setListUserUnregister] = useState();
    const [listUserWait, setListUserWait] = useState();
    const [listUserDone, setListUserDone] = useState();
    const [listFindUserWait, setListFindUserWait] = useState([]);
    const [listFindUserDone, setListFindUserDone] = useState([]);
    const dispatch = useDispatch();
    const registerCourse = useSelector(
        (state) => state.elearnningReducer.registerCourse
    );
    const listUser = useSelector((state) => state.elearnningReducer.listUser);
    //   console.log(props);

    useEffect(() => {
        courseAPI
            .getListUserDone({ maKhoaHoc: `${props.course.maKhoaHoc}` })
            .then((res) => {
                setListUserDone(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        courseAPI
            .getListUserUnregister({ maKhoaHoc: `${props.course.maKhoaHoc}` })
            .then((res) => {
                setListUserUnregister(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        courseAPI
            .getListUserWait({ maKhoaHoc: `${props.course.maKhoaHoc}` })
            .then((res) => {
                setListUserWait(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [registerCourse]);

    const handleDeleteCourseDone = (taiKhoan) => {
        return dispatch(
            deleteCourseUserThunk({
                maKhoaHoc: `${props.course.maKhoaHoc}`,
                taiKhoan: `${taiKhoan}`,
            })
        );
    };

    const renderListUserWait = (data) => {
        return data?.map((user, index) => {
            return {
                stt: index + 1,
                taiKhoan: user.taiKhoan,
                hoTen: user.hoTen,
            };
        });
    };

    const renderListUserDone = (data) => {
        return data?.map((user, index) => {
            return {
                stt: index + 1,
                taiKhoan: user.taiKhoan,
                hoTen: user.hoTen,
                choXacNhan: (
                    <button
                        className="border rounded bg-green-700 p-1 text-white"
                        id={user}
                        onClick={() => {
                            handleDeleteCourseDone(user.taiKhoan);
                        }}
                    >
                        Huỷ ghi danh
                    </button>
                ),
            };
        });
    };

    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const onFinish = (values) => {
        console.log({
            taiKhoan: `${values.taiKhoan}`,
            maKhoaHoc: `${props.course.maKhoaHoc}`,
        });
        dispatch(
            registerCourseThunk({
                taiKhoan: `${values.taiKhoan}`,
                maKhoaHoc: `${props.course.maKhoaHoc}`,
            })
        );
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    // INPUT SEARCH ANTD
    const onChange = (value) => {
        console.log(`selected ${value}`);
    };
    const onSearch = (value) => {
        console.log('search:', value);
    };

    let arrayChangeWait = [];
    for (let i = 0; i < listUserWait?.length; i++) {
        for (let k = 0; k < listUser?.length; k++) {
            if (
                listUserWait[i].taiKhoan === listUser[k].taiKhoan &&
                listUserWait[i].hoTen === listUser[k].hoTen
            ) {
                arrayChangeWait.push(listUser[k]);
            }
        }
    }

    const onChangeWait = (e) => {
        let keyword = e.target.value.trim().toUpperCase();
        console.log(keyword);
        let result = arrayChangeWait.find(
            (user) => user.hoTen.trim().toUpperCase() === keyword || user.soDt === keyword
        );
        console.log(result);
        setListFindUserWait([result]);
    };

    let arrayChangeDone = [];
    for (let i = 0; i < listUserDone?.length; i++) {
        for (let k = 0; k < listUser?.length; k++) {
            if (
                listUserDone[i].taiKhoan === listUser[k].taiKhoan &&
                listUserDone[i].hoTen === listUser[k].hoTen
            ) {
                arrayChangeDone.push(listUser[k]);
            }
        }
    }

    const onChangeDone = (e) => {
        let keyword = e.target.value.trim().toUpperCase();
        console.log(keyword);
        console.log(arrayChangeDone);
        let result = arrayChangeDone.find(
            (user) => user.hoTen.trim().toUpperCase() === keyword || user.soDt === keyword
        );
        console.log(result);
        setListFindUserDone([result]);
    };

    const renderRecommendInput = () => {
        return listUserUnregister?.map((user) => {
            return {
                value: user.taiKhoan,
                label: user.hoTen,
            };
        });
    };

    // TABLE ANTD
    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            align: 'center',
        },
        {
            title: 'Tên tài khoản',
            dataIndex: 'taiKhoan',
            align: 'center',
        },
        {
            title: 'Học viên',
            dataIndex: 'hoTen',
            align: 'center',
        },
        {
            title: 'Chờ xác nhận',
            dataIndex: 'choXacNhan',
            align: 'center',
        },
    ];
    //   console.log(listFindUserWait);
    // DATA WAITING COURSE

    //   const dataWait = renderListUserWait(listUserWait);
    const dataWait = () => {
        if (
            listFindUserWait == '' ||
            listFindUserWait == null ||
            listFindUserWait == undefined
        ) {
            return renderListUserWait(listUserWait);
        } else {
            return renderListUserWait(listFindUserWait);
        }
    };

    // DATA DONE COURSE

    const dataDone = () => {
        if (
            listFindUserDone == '' ||
            listFindUserDone == null ||
            listFindUserDone == undefined
        ) {
            return renderListUserDone(listUserDone);
        } else {
            return renderListUserDone(listFindUserDone);
        }
    };
    return (
        <div>
            <Button
                className="border bg-green-500 h-10 w-22"
                type=""
                onClick={showModal}
            >
                GHI DANH
            </Button>
            <Modal
                footer=""
                title=""
                open={isModalOpen}
                onOk={handleOk}
                onCancel={handleCancel}
                width={800}
            >
                <div className="">
                    <Form
                        name="nest-messages"
                        onFinish={onFinish}
                        className="flex space-x-2 border-b-2"
                        size="large"
                        style={{
                            maxWidth: 700,
                        }}
                        // validateMessages={validateMessages}
                    >
                        <h1 className=" text-xl font-bold mb-3 text-white">
                            Chọn người dùng
                        </h1>

                        <Form.Item
                            name="taiKhoan"
                            style={{ width: 400 }}
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng chọn học viên đăng ký!',
                                },
                            ]}
                        >
                            <Select
                                showSearch
                                placeholder="Chọn người dùng"
                                optionFilterProp="children"
                                onChange={onChange}
                                onSearch={onSearch}
                                filterOption={(input, option) =>
                                    (option?.label ?? '')
                                        .toLowerCase()
                                        .includes(input.toLowerCase())
                                }
                                options={renderRecommendInput()}
                            />
                            {/* <Input placeholder="Tên người dùng" className="" /> */}
                        </Form.Item>

                        <Form.Item
                            wrapperCol={{
                                ...layout.wrapperCol,
                                offset: 6,
                            }}
                        >
                            <Button
                                type="primary"
                                htmlType="submit"
                                className="buttonLogin"
                            >
                                GHI DANH
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                <div className="flex justify-between my-3">
                    <p className="font-bold text-lg text-white">HỌC VIÊN CHỜ XÁC THỰC</p>
                    <Input
                        placeholder="Nhập tên học viên hoặc số điện thoại"
                        style={{ width: '40%' }}
                        onChange={onChangeWait}
                    />
                </div>
                <Table
                    columns={columns}
                    style={{backgroundColor:"white"}}
                    dataSource={dataWait()}
                    size="middle"
                    pagination={{ pageSize: 5 }}
                />

                <div className="flex justify-between my-3">
                    <p className="font-bold text-lg text-white">
                        HỌC VIÊN ĐÃ THAM GIA KHOÁ HỌC
                    </p>
                    <Input
                        placeholder="Nhập tên học viên hoặc số điện thoại"
                        style={{ width: '40%' }}
                        onChange={onChangeDone}
                    />
                </div>
                <Table
                    columns={columns}
                    style={{backgroundColor:"white"}}
                    dataSource={dataDone()}
                    size="middle"
                    pagination={{ pageSize: 5
                         }}
                />
            </Modal>
        </div>
    );
}
