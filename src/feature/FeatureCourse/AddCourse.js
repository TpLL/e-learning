import React from "react";
import { Button, Form, Input, Select } from "antd";
import { addCourseThunk } from "../../app/elearningReducer";
import { useDispatch, useSelector } from "react-redux";
import TypedInputNumber from "antd/es/input-number";
import { Upload } from "antd";
import TextArea from "antd/es/input/TextArea";
import { useState } from "react";

export default function AddCourse() {
  const valueUserLogin = useSelector(
    (state) => state.elearnningReducer.valueUserLogin
  );
  console.log(valueUserLogin);
  const dispatch = useDispatch();
  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const onFinish = (values) => {
    console.log(values);
    dispatch(addCourseThunk(values));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  // UPLOAD ANTD

  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList.map((img) => {
      return img.name;
    });
  };

  return (
    <div className="">
      <h1 className="text-center text-3xl font-bold mb-3 text-red-600">THÊM KHOÁ HỌC</h1>
      <Form
        className="grid grid-cols-2 space-x-2"
        name="nest-messages"
        onFinish={onFinish}
        style={{
          maxWidth: 800,
        }}
      >
        <Form.Item
          name="maKhoaHoc"
          className="ml-2"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập Khoá học!",
            },
          ]}
        >
          <Input placeholder="Khoá học" />
        </Form.Item>
        <Form.Item
          name="tenKhoaHoc"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên Khoá học!",
            },
          ]}
        >
          <Input placeholder="Tên khoá học" className="" />
        </Form.Item>

        <Form.Item
          name="maDanhMucKhoaHoc"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập Danh mục khoá học!",
            },
          ]}
        >
          <Select className="" placeholder="Danh mục khoá học">
            <Select.Option value="BackEnd">
              Lập trình Backend-BackEnd
            </Select.Option>
            <Select.Option value="Design">Thiết kế Web-Design</Select.Option>
            <Select.Option value="DiDong">
              Lập trình di động-DiDong
            </Select.Option>
            <Select.Option value="FrontEnd">
              Lập trình Front end-FrontEnd
            </Select.Option>
            <Select.Option value="FullStack">
              Lập trình Full Stack-FullStack
            </Select.Option>
            <Select.Option value="TuDuy">Tư duy lập trình-TuDuy</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          name="ngayTao"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập Ngày tạo!",
            },
          ]}
        >
          <Input placeholder="Ngày tạo" className="" />
        </Form.Item>

        <Form.Item
          name="danhGia"
          rules={[
            {
              required: true,
              message:
                "Vui lòng nhập Đánh giá, thấp nhất là 0 và cao nhất là 5",
            },
          ]}
        >
          <TypedInputNumber
            placeholder="Đánh giá"
            className=""
            min={1}
            max={5}
          />
        </Form.Item>

        <Form.Item
          name="luotXem"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập Lượt xem!",
            },
          ]}
        >
          <TypedInputNumber placeholder="Lượt xem" className="" min={1} />
        </Form.Item>

        <Form.Item
          name="taiKhoanNguoiTao"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập Người tạo!",
            },
          ]}
        >
          <Select className="" placeholder="Người tạo">
            <Select.Option value={valueUserLogin?.taiKhoan}>
              {valueUserLogin?.hoTen}
            </Select.Option>
          </Select>
        </Form.Item>

        <Form.Item label="Upload" name="hinhAnh" getValueFromEvent={normFile}>
          <Upload
            // action="/upload.do"
            listType="picture-card"
            accept=".png, .jpg"
          >
            <div>
              <div
                style={{
                  marginTop: 8,
                }}
              >
                Upload
              </div>
            </div>
          </Upload>
        </Form.Item>

        <Form.Item
          name="maNhom"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập Mã nhóm!",
            },
          ]}
        >
          <Select className="" placeholder="Mã nhóm">
            <Select.Option value="GP01">GP01</Select.Option>
            {/* <Select.Option value="GP02">GP02</Select.Option>
            <Select.Option value="GP03">GP03</Select.Option>
            <Select.Option value="GP04">GP04</Select.Option>
            <Select.Option value="GP05">GP05</Select.Option>
            <Select.Option value="GP06">GP06</Select.Option>
            <Select.Option value="GP07">GP07</Select.Option>
            <Select.Option value="GP08">GP08</Select.Option>
            <Select.Option value="GP09">GP09</Select.Option>
            <Select.Option value="GP010">GP10</Select.Option>
            <Select.Option value="GP011">GP11</Select.Option>
            <Select.Option value="GP012">GP12</Select.Option>
            <Select.Option value="GP013">GP13</Select.Option>
            <Select.Option value="GP014">GP14</Select.Option>
            <Select.Option value="GP015">GP15</Select.Option> */}
          </Select>
        </Form.Item>

        <div className="col-span-2">
          <Form.Item
            className=""
            name="moTa"
            rules={[
              {
                required: true,
                message: "Mô tả không được để trống!",
              },
            ]}
          >
            <div className="flex">
              <img
                src="https://demo2.cybersoft.edu.vn/Img/ImgLogo/logo512.png"
                style={{ height: 200 }}
              ></img>
              <TextArea placeholder="Nhập mô tả" rows={4} />
            </div>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              ...layout.wrapperCol,
              offset: 8,
            }}
          >
            <Button type="primary" htmlType="submit" className="buttonLogin">
              <span className="text-xl"> Thêm khoá học</span>
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
}
