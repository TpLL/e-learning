import React from "react";
import { Modal } from "antd";
import { useState } from "react";
import TypedInputNumber from "antd/es/input-number";
import TextArea from "antd/es/input/TextArea";
import { Button, Form, Input, Select, Upload } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { fixCourseThunk } from "../../app/elearningReducer";
export default function FixCourse(props) {
  const valueUserLogin = useSelector(
    (state) => state.elearnningReducer.valueUserLogin
  );
  const dispatch = useDispatch();
  const [isModalOpenUpdate, setIsModalOpenUpdate] = useState(false);

  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };

  const onFinish = (values) => {
    console.log(values);

    dispatch(fixCourseThunk(values));
  };

  // UPLOAD ANTD

  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList.map((img) => {
      return img.name;
    });
  };

  const renderSelect = () => {
    if (valueUserLogin) {
      return (
        <Select.Option value={valueUserLogin.taiKhoan}>
          {valueUserLogin.hoTen}
        </Select.Option>
      );
    } else {
      <Select.Option value=""></Select.Option>;
    }
  };

  const showModalUpdate = () => {
    setIsModalOpenUpdate(true);
  };
  const handleOkUpdate = () => {
    setIsModalOpenUpdate(true);
  };
  const handleCancelUpdate = () => {
    setIsModalOpenUpdate(false);
  };

  return (
    <div>
      <p onClick={showModalUpdate} className="p-2">
        Sửa
      </p>
      <Modal
        open={isModalOpenUpdate}
        onOk={handleOkUpdate}
        onCancel={handleCancelUpdate}
        footer={null}
        width={900}
      >
        <h1 className="text-center text-3xl font-bold mb-3 text-white">
          CẬP NHẬT KHOÁ HỌC
        </h1>

        <Form
          className="grid grid-cols-2 space-x-2"
          name="nest-messages"
          onFinish={onFinish}
          style={{
            maxWidth: 800,
          }}
        >
          <Form.Item
            name="maKhoaHoc"
            className="ml-2"
            initialValue={props.course.maKhoaHoc}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập Khoá học!",
              },
            ]}
          >
            <Input placeholder="Khoá học" />
          </Form.Item>
          <Form.Item
            name="tenKhoaHoc"
            initialValue={props.course.tenKhoaHoc}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên Khoá học!",
              },
            ]}
          >
            <Input placeholder="Tên khoá học" className="" />
          </Form.Item>

          <Form.Item
            name="maDanhMucKhoaHoc"
            // initialValue={props.course.danhMucKhoaHoc.tenDanhMucKhoaHoc}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập Danh mục khoá học!",
              },
            ]}
          >
            <Select className="" placeholder="Danh mục khoá học">
              <Select.Option value="BackEnd">
                Lập trình Backend-BackEnd
              </Select.Option>
              <Select.Option value="Design">Thiết kế Web-Design</Select.Option>
              <Select.Option value="DiDong">
                Lập trình di động-DiDong
              </Select.Option>
              <Select.Option value="FrontEnd">
                Lập trình Front end-FrontEnd
              </Select.Option>
              <Select.Option value="FullStack">
                Lập trình Full Stack-FullStack
              </Select.Option>
              <Select.Option value="TuDuy">
                Tư duy lập trình-TuDuy
              </Select.Option>
            </Select>
          </Form.Item>

          <Form.Item
            name="ngayTao"
            initialValue={props.course.ngayTao}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập Ngày tạo!",
              },
            ]}
          >
            <Input placeholder="Ngày tạo" className="" />
          </Form.Item>

          <Form.Item
            name="danhGia"
            rules={[
              {
                required: true,
                message:
                  "Vui lòng nhập Đánh giá, thấp nhất là 0 và cao nhất là 5",
              },
            ]}
          >
            <TypedInputNumber
              placeholder="Đánh giá"
              className=""
              min={1}
              max={5}
            />
          </Form.Item>

          <Form.Item
            name="luotXem"
            initialValue={props.course.luotXem}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập Lượt xem!",
              },
            ]}
          >
            <TypedInputNumber placeholder="Lượt xem" className="" min={1} />
          </Form.Item>

          <Form.Item
            name="taiKhoanNguoiTao"
            // initialValue={props.course.nguoiTao.hoTen}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập Người tạo!",
              },
            ]}
          >
            <Select className="" placeholder="Người tạo">
              {renderSelect()}
              {/* <Select.Option value={valueUserLogin.taiKhoan}>
                {valueUserLogin.hoTen}
              </Select.Option> */}
            </Select>
          </Form.Item>

          <Form.Item
            label="Upload"
            name="hinhAnh"
            getValueFromEvent={normFile}
            initialValue={props.course.hinhAnh}
          >
            <Upload action="/upload.do" listType="picture-card">
              <div>
                <div
                  style={{
                    marginTop: 8,
                  }}
                >
                  Upload
                </div>
              </div>
            </Upload>
          </Form.Item>

          <Form.Item
            name="maNhom"
            initialValue={props.course.maNhom}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập Mã nhóm!",
              },
            ]}
          >
            <Select className="" placeholder="Mã nhóm">
              <Select.Option value="GP01">GP01</Select.Option>
              <Select.Option value="GP02">GP02</Select.Option>
              <Select.Option value="GP03">GP03</Select.Option>
              <Select.Option value="GP04">GP04</Select.Option>
              <Select.Option value="GP05">GP05</Select.Option>
              <Select.Option value="GP06">GP06</Select.Option>
              <Select.Option value="GP07">GP07</Select.Option>
              <Select.Option value="GP08">GP08</Select.Option>
              <Select.Option value="GP09">GP09</Select.Option>
              <Select.Option value="GP010">GP10</Select.Option>
              <Select.Option value="GP011">GP11</Select.Option>
              <Select.Option value="GP012">GP12</Select.Option>
              <Select.Option value="GP013">GP13</Select.Option>
              <Select.Option value="GP014">GP14</Select.Option>
              <Select.Option value="GP015">GP15</Select.Option>
            </Select>
          </Form.Item>

          <div className="col-span-2">
            <Form.Item
              className=""
              name="moTa"
              initialValue={props.course.moTa}
              rules={[
                {
                  required: true,
                  message: "Mô tả không được để trống!",
                },
              ]}
            >
              <div className="flex">
                <img
                  src="https://demo2.cybersoft.edu.vn/Img/ImgLogo/logo512.png"
                  style={{ height: 200 }}
                ></img>
                <TextArea placeholder="Nhập mô tả" rows={4} />
              </div>
            </Form.Item>
            <Form.Item
              wrapperCol={{
                ...layout.wrapperCol,
                offset: 8,
              }}
            >
              <Button type="primary" htmlType="submit" className="buttonLogin">
                <span className="text-xl">Cập nhật</span>
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </div>
  );
}
