import React from 'react';
import { Modal, message } from 'antd';
import { useState } from 'react';
import { Button, Form, Input, Select } from 'antd';
import { useSelector } from 'react-redux';
import { userAPI } from '../../services/QuanLyAPI/severAPI';

export default function UpdatePageQLKH() {
    const valueUserLogin = useSelector(
        (state) => state.elearnningReducer.valueUserLogin
    );
    console.log(valueUserLogin);
    const [isModalOpenUpdate, setIsModalOpenUpdate] = useState(false);

    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };

    const onFinishUpdate = (values) => {
        console.log(values);
        userAPI
            .updateInforUser(values)
            .then((res) => {
                console.log(res);
                message.success('Cập nhật thành công!');
                // userLocal.set(res.data);
            })
            .catch((err) => {
                console.log(err);
                message.error(err.response.data);
            });
    };

    const showModalUpdate = () => {
        setIsModalOpenUpdate(true);
    };
    const handleOkUpdate = () => {
        setIsModalOpenUpdate(true);
    };
    const handleCancelUpdate = () => {
        setIsModalOpenUpdate(false);
    };
    return (
        <div>
            <p onClick={showModalUpdate}>Thông tin người dùng</p>
            <Modal
                open={isModalOpenUpdate}
                onOk={handleOkUpdate}
                onCancel={handleCancelUpdate}
                footer={null}
            >
                <Form
                    name="nest-messages"
                    onFinish={onFinishUpdate}
                    style={{
                        maxWidth: 500,
                    }}
                    // validateMessages={validateMessages}
                >
                    <h1 className="text-center text-3xl font-bold mb-3">
                        THÔNG TIN NGƯỜI DÙNG
                    </h1>
                    <Form.Item
                        error="Tài khoản"
                        name="taiKhoan"
                        initialValue={valueUserLogin.taiKhoan}
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập Tài khoản!',
                            },
                        ]}
                    >
                        <Input placeholder="Tài khoản" className="" />
                    </Form.Item>
                    <Form.Item
                        name="hoTen"
                        initialValue={valueUserLogin.hoTen}
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập Họ tên!',
                            },
                        ]}
                    >
                        <Input placeholder="Họ tên" className="" />
                    </Form.Item>
                    <Form.Item
                        name="matKhau"
                        initialValue={valueUserLogin.matKhau}
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập Mật khẩu!',
                            },
                        ]}
                    >
                        <Input
                            type="password"
                            placeholder="Mật khẩu"
                            className=""
                        />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        initialValue={valueUserLogin.email}
                        rules={[
                            {
                                type: 'email',
                                required: true,
                                message: 'Vui lòng nhập Email!',
                            },
                        ]}
                    >
                        <Input placeholder="Email" className="" />
                    </Form.Item>
                    <Form.Item
                        name="soDT"
                        initialValue={valueUserLogin.soDt}
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập Số điện thoại!',
                            },
                        ]}
                    >
                        <Input placeholder="Số điện thoại" className="" />
                    </Form.Item>
                    <Form.Item
                        name="maLoaiNguoiDung"
                        initialValue={valueUserLogin.maLoaiNguoiDung}
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập Loại người dùng!',
                            },
                        ]}
                    >
                        <Select className="" placeholder="Loại người dùng">
                            <Select.Option value="GV">Giáo vụ</Select.Option>
                            <Select.Option value="HV">Học viên</Select.Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        name="maNhom"
                        initialValue={valueUserLogin.maNhom}
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập mã Nhóm!',
                            },
                        ]}
                    >
                        <Select className="" placeholder="Mã nhóm">
                            <Select.Option value="GP01">GP01</Select.Option>
                            <Select.Option value="GP02">GP02</Select.Option>
                            <Select.Option value="GP03">GP03</Select.Option>
                            <Select.Option value="GP04">GP04</Select.Option>
                            <Select.Option value="GP05">GP05</Select.Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            ...layout.wrapperCol,
                            offset: 8,
                        }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="buttonLogin"
                        >
                            Cập nhật
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
