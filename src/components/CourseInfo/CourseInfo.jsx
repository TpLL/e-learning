import React from 'react';
import './CourseInfoStyle.css';
const CourseInfo = () => {
    return (
        <div className="course_info p-[50px] text-white">
            <div className="grid grid-cols-3 gap-5">
                <div className="infoItemHome md:col-span-3 infoLargeItem">
                    <h1 className="text-[28px] sm:text-2xl font-medium">
                        KHÓA HỌC
                    </h1>
                    <p className="sm:text-xs">
                        <span className="font-bold">Học qua dự án thực tế</span>
                        , học đi đôi với hành, không lý thuyết lan man, phân
                        tích cội nguồn của vấn đề, xây dựng từ các ví dụ nhỏ đến
                        thực thi một dự án lớn ngoài thực tế để học viên học
                        xong làm được ngay
                    </p>
                    <ul className="mt-3 space-y-2 sm:text-xs">
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>Hơn 1000 bài tập và dự án thực tế</span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>Công nghệ cập nhật mới nhất</span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Hình ảnh, ví dụ, bài giảng sinh động trực quan
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Tư duy phân tích, giải quyết vấn đề trong dự án
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Học tập kinh nghiệm, qui trình làm dự án, các
                                qui chuẩn trong dự án
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Cơ hội thực tập tại các công ty lớn như FPT,
                                Microsoft
                            </span>
                        </li>
                    </ul>
                </div>
                <div className="infoItemHome md:col-span-3 bg-[#f6ba35]">
                    <h1 className="text-[28px] sm:text-2xl font-medium mb-1">
                        LỘ TRÌNH PHÙ HỢP
                    </h1>
                    <ul className="space-y-2 sm:text-xs">
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Lộ trình bài bản từ zero tới chuyên nghiệp, nâng
                                cao
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Học, luyện tập code, kỹ thuật phân tích, soft
                                skill
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Huấn luyện để phát triển năng lực và niềm đam mê
                                lập trình
                            </span>
                        </li>
                    </ul>
                </div>
                <div className="infoItemHome md:col-span-3 bg-[#5c8295]">
                    <h1 className="text-[28px] sm:text-2xl font-medium mb-1">
                        HỆ THỐNG HỌC TẬP
                    </h1>
                    <ul className="space-y-2 sm:text-xs">
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Tự động chấm điểm trắc nghiệm và đưa câu hỏi tùy
                                theo mức độ học viên
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Thống kê lượt xem video, làm bài, điểm số theo
                                chu kỳ
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Thống kê, so sánh khả năng học của các học viên
                                cùng level để đưa ra mục tiêu học tập
                            </span>
                        </li>
                    </ul>
                </div>
                <div className="infoItemHome md:col-span-3 bg-[#f6ba35]">
                    <h1 className="text-[28px] sm:text-2xl font-medium mb-1">
                        GIẢNG VIÊN
                    </h1>
                    <ul className="space-y-2 sm:text-xs">
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Tương tác cùng mentor và giảng viên qua phần
                                thảo luận
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Review code và đưa ra các nhận xét góp ý
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Chấm điểm tương tác thảo luận giữa các học viên
                            </span>
                        </li>
                    </ul>
                </div>
                <div className="infoItemHome md:col-span-3 bg-[#63c0a8]">
                    <h1 className="text-[28px] sm:text-2xl font-medium mb-1">
                        CHỨNG NHẬN
                    </h1>
                    <ul className="space-y-2 sm:text-xs">
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Chấm bài và có thể vấn đáp trực tuyến để review
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Hệ thống của chúng tôi cũng tạo ra cho bạn một
                                CV trực tuyến độc đáo
                            </span>
                        </li>
                        <li className="px-[5px]">
                            <i className="fa-solid fa-check iconCheck mr-2.5"></i>
                            <span>
                                Kết nối CV của bạn đến với các đối tác của V
                                learning
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default CourseInfo;
