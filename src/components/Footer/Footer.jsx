import React from 'react';
import './footerStyle.css';
import { useEffect } from 'react';
import { useState } from 'react';
import { useRef } from 'react';

const Footer = () => {
    const lastScrollTop = useRef(0);
    const [isNavbarVisible, setIsNavbarVisible] = useState(false);

    const handleScroll = () => {
        lastScrollTop.current = window.pageYOffset;

        if (lastScrollTop.current > 200) {
            setIsNavbarVisible(true);
        } else {
            setIsNavbarVisible(false);
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <div className="footer p-2.5">
            <div className="footer_body py-2.5 px-10 border-b border-zinc-300">
                <div className="grid grid-cols-6 sm:grid-cols-4">
                    <div className="col-span-2 p-2.5 md:p-2 md:pl-0 pl-0 sm:col-span-2 xs:col-span-4">
                        <a
                            href="#!"
                            className="mb-3 footer_logo relative hover:scale-105 duration-300 cursor-pointer"
                        >
                            <span className="textV mr-1">V</span>
                            learning
                            <i className="fa-regular fa-keyboard absolute top-4 right-2"></i>
                        </a>
                        <ul className="footer_contact space-y-3 ml-4">
                            <li className="space-x-3 cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-phone"></i>
                                <span className="font-medium sm:text-xs">
                                    1800-123-4567
                                </span>
                            </li>
                            <li className="space-x-3 flex items-center cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-envelope-open-text"></i>
                                <span className="font-medium sm:text-xs">
                                    devit@gmail.com
                                </span>
                            </li>
                            <li className="space-x-3 cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-location-dot"></i>
                                <span className="font-medium sm:text-xs">
                                    Đà Nẵng
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div className="col-span-1 p-2.5 md:p-2 sm:col-span-1 xs:col-span-2">
                        <h3 className="text-[25px] font-semibold mb-2 md:text-2xl">
                            Liên kết
                        </h3>
                        <ul className="space-y-2">
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Trang chủ
                            </li>
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Dịch vụ
                            </li>
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Nhóm
                            </li>
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Blog
                            </li>
                        </ul>
                    </div>
                    <div className="col-span-1 py-2.5 md:p-2 md:pr-0 sm:col-span-1 xs:col-span-2">
                        <h3 className="text-[25px] font-semibold mb-2 md:text-2xl">
                            Khóa học
                        </h3>
                        <ul className="space-y-2">
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Front End
                            </li>
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Back End
                            </li>
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Full Stack
                            </li>
                            <li className="font-medium sm:text-xs cursor-default hover:-translate-y-[2px] hover:text-[#01877e] duration-300">
                                <i className="fa-solid fa-chevron-right text-xs mr-2"></i>
                                Node Js
                            </li>
                        </ul>
                    </div>
                    <div className="col-span-2 p-2.5 md:p-2 pr-0 sm:col-span-3">
                        <h3 className="text-[25px] sm:text-2xl font-semibold mb-2">
                            Đăng kí tư vấn
                        </h3>
                        <form action="">
                            <input
                                className="formFooter"
                                type="text"
                                placeholder="Họ và tên"
                            />
                            <input
                                className="formFooter"
                                type="text"
                                placeholder="Email"
                            />
                            <input
                                className="formFooter"
                                type="text"
                                placeholder="Số điện thoại"
                            />
                        </form>
                        <button className="custom-btn btnGlobal mt-3 hover:scale-95 duration-300">
                            Đăng kí
                        </button>
                    </div>
                </div>
            </div>

            <div className="extraFooter py-2.5 sm:py-[5px] px-10 flex justify-between items-center sm:flex-col sm:items-start">
                <div>
                    <p className="sm:text-xs">
                        Copyright © 2021. All rights reserved.
                    </p>
                </div>
                <div className="footer-social space-x-3 sm:mt-1">
                    <i className="fa-brands fa-instagram"></i>
                    <i className="fa-brands fa-facebook-f"></i>
                    <i className="fa-brands fa-twitter"></i>
                </div>
            </div>

            <div
                className={`btt_button fixed bottom-6 right-6 cursor-pointer duration-300 ${
                    isNavbarVisible ? 'opacity-100' : 'opacity-0'
                }`}
            >
                <div
                    className="w-10 h-10 rounded-md bg-[#41b294] flex justify-center items-center"
                    onClick={() => {
                        window.scrollTo(0, 0);
                    }}
                >
                    <i className="fa-solid fa-arrow-up text-white"></i>
                </div>
            </div>
        </div>
    );
};

export default Footer;
