import React from 'react';
import './headerStyle.css';
import { Desktop, Mobile, Tablet } from '../../layout/responsive';
import HeaderDesktop from './HeaderDesktop';
import HeaderMobile from './HeaderMobile';
import HeaderTablet from './HeaderTablet';

const Header = () => {
    return (
        <div>
            <Desktop>
                <HeaderDesktop />
            </Desktop>
            <Tablet>
                <HeaderTablet />
            </Tablet>
            <Mobile>
                <HeaderMobile />
            </Mobile>
        </div>
    );
};

export default Header;
