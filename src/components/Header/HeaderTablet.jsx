import React, { useEffect, useRef, useState } from 'react';
import './headerStyle.css';
import logo from '../../assets/img/logo.png';
import avatar from '../../assets/img/02.webp';
import { courseServ } from '../../services/courseServ';
import { NavLink, useNavigate } from 'react-router-dom';
import { userLocal } from '../../services/QuanLyAPI/localUser';

const HeaderTablet = () => {
    const [coursesCate, setCoursesCate] = useState();
    const [isNavbarVisible, setIsNavbarVisible] = useState(true);
    const [isShowNav, setIsShowNav] = useState(false);
    const [isTop, setIsTop] = useState(true);
    const lastScrollTop = useRef(0);
    const navigate = useNavigate();

    const handleScroll = () => {
        const { pageYOffset } = window;
        setIsShowNav(false);

        if (pageYOffset < 100) setIsTop(true);
        else setIsTop(false);

        if (pageYOffset > lastScrollTop.current) {
            setIsNavbarVisible(false);
        } else if (pageYOffset < lastScrollTop.current) {
            setIsNavbarVisible(true);
        }
        lastScrollTop.current = pageYOffset;
    };

    useEffect(() => {
        courseServ
            .getCoursesCate()
            .then((res) => {
                setCoursesCate(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    const renderCourseCateList = () => {
        return (
            coursesCate &&
            coursesCate.map((item, index) => {
                return (
                    <NavLink
                        key={index}
                        to={`/danhmuckhoahoc/${item.maDanhMuc}`}
                    >
                        <li className="sub-menu_item">
                            {item.tenDanhMuc.toUpperCase()}
                        </li>
                    </NavLink>
                );
            })
        );
    };

    const renderUserMenu = () => {
        if (!userLocal.get()) {
            return (
                <NavLink to="/login">
                    <button className="bg-[#f6ba35] px-2.5 py-[5px] text-white text-[15px] hover:scale-95 duration-300">
                        ĐĂNG NHẬP
                    </button>
                </NavLink>
            );
        } else {
            return (
                <div className="userMenu relative flex gap-2 items-center">
                    <img
                        src={avatar}
                        alt=""
                        className="relative w-[50px] h-[50px] rounded-full object-cover z-50 cursor-pointer"
                        onClick={() => {
                            navigate('/thongtincanhan');
                        }}
                    />

                    <span
                        className="-translate-y-2 p-3 pl-0 cursor-pointer"
                        onClick={() => {
                            setIsShowNav(!isShowNav);
                        }}
                    >
                        <span>
                            <i className="fa-solid fa-sort-down text-2xl"></i>
                            <div
                                id="navigate_mobile"
                                className={`nav_mobile absolute top-[70px] ${
                                    isShowNav
                                        ? 'right-[-50px]'
                                        : 'right-[-250px]'
                                } w-[180px] h-[300px] bg-[rgba(1,135,126,.8)] duration-300`}
                            >
                                <ul className="menu flex flex-col w-full">
                                    <li className="menu_item menu_item_search font-medium text-center p-[5px] leading-8">
                                        <form action="" onSubmit={handleSearch}>
                                            <input
                                                type="text"
                                                placeholder="Tìm kiếm"
                                                className="search_input_mobile w-full px-2 py-[2px]"
                                            />
                                        </form>
                                    </li>
                                    <li className="menu_item course_cate flex items-center justify-center text-center p-[5px] leading-8">
                                        <i className="fa-solid fa-bars mr-2 text-base"></i>
                                        <span className="font-medium">
                                            DANH MỤC
                                        </span>
                                        <ul className="sub-menu">
                                            {renderCourseCateList()}
                                        </ul>
                                    </li>
                                    <NavLink to="/khoahoc">
                                        <li className="menu_item font-medium text-center p-[5px] leading-8">
                                            KHÓA HỌC
                                        </li>
                                    </NavLink>
                                    <NavLink to="/blog">
                                        <li className="menu_item font-medium text-center p-[5px] leading-8">
                                            BLOG
                                        </li>
                                    </NavLink>
                                    <NavLink to="/sukien">
                                        <li className="menu_item font-medium event text-center p-[5px] leading-8">
                                            <span>SỰ KIỆN</span>
                                            <ul className="sub-menu">
                                                <li className="sub-menu_item">
                                                    SỰ KIỆN SALE CUỐI NĂM
                                                </li>
                                                <li className="sub-menu_item">
                                                    SỰ KIỆN GIÁNG SINH
                                                </li>
                                                <li className="sub-menu_item">
                                                    SỰ KIỆN NOEL
                                                </li>
                                            </ul>
                                        </li>
                                    </NavLink>
                                    <NavLink to="/thongtin">
                                        <li className="menu_item font-medium text-center p-[5px] leading-8">
                                            THÔNG TIN
                                        </li>
                                    </NavLink>
                                    <li
                                        className="menu_item font-medium text-center p-[5px] leading-8"
                                        onClick={() => {
                                            userLocal.remove();
                                            window.location.href = '/';
                                        }}
                                    >
                                        ĐĂNG XUẤT
                                    </li>
                                </ul>
                            </div>
                        </span>
                    </span>
                </div>
            );
        }
    };

    const handleSearch = (event) => {
        event.preventDefault();
        window.location.href = `/timkiem/${event.target[0].value}`;
    };

    return (
        <div
            className={`header header_tablet ${
                isNavbarVisible ? '' : '-translate-y-24'
            } ${
                isTop ? 'py-2.5' : 'py-[5px] header_notTop'
            } fixed top-0 left-0 right-0 bg-white duration-300 z-40`}
        >
            <div className="px-[50px] flex items-center justify-between">
                <div className="flex items-center">
                    <NavLink
                        to="/"
                        onClick={() => {
                            window.scrollTo(0, 0);
                        }}
                    >
                        <div className="flex items-center gap-2 hover:scale-105 duration-300 mr-20">
                            <img src={logo} alt="" className="w-[70px] " />
                            <p className="flex items-end">
                                <span className="text-[#f6ba35] text-5xl font-bold">
                                    <i className="fa-brands fa-vimeo-v"></i>
                                </span>
                                <span className="text-lg font-semibold">
                                    LEARNING
                                </span>
                            </p>
                        </div>
                    </NavLink>
                </div>

                <div className="userMenu">{renderUserMenu()}</div>
            </div>
        </div>
    );
};

export default HeaderTablet;
