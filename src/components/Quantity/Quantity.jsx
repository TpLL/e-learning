import React, { useState } from 'react';
import quantity1 from '../../assets/img/quantity1.png';
import quantity2 from '../../assets/img/quantity2.png';
import quantity3 from '../../assets/img/quantity3.png';
import quantity4 from '../../assets/img/quantity4.png';
import CountUp from 'react-countup';
import { useInView } from 'react-intersection-observer';

const Quantity = () => {
    const { ref, inView } = useInView();

    const formatNumber = (value) => {
        return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '');
    };

    return (
        <div className="my-12 p-[50px] bg-[#f0f8ff] flex flex-wrap sm:flex-col">
            <div className="basis-1/4 md:basis-1/2 p-6 flex flex-col items-center">
                <img src={quantity4} alt="" className="w-20 h-20" />
                <span
                    ref={ref}
                    className="text-[#41b294] text-[50px] font-extrabold"
                >
                    {
                        <CountUp
                            end={9000}
                            duration={5}
                            formattingFn={formatNumber}
                        />
                    }
                </span>
                <span className="font-medium">Học viên</span>
            </div>
            <div className="basis-1/4 md:basis-1/2 p-6 flex flex-col items-center">
                <img src={quantity3} alt="" className="w-20 h-20" />
                <span
                    ref={ref}
                    className="text-[#41b294] text-[50px] font-extrabold"
                >
                    {
                        <CountUp
                            end={1000}
                            duration={5}
                            formattingFn={formatNumber}
                        />
                    }
                </span>
                <span className="font-medium">Học viên</span>
            </div>
            <div className="basis-1/4 md:basis-1/2 p-6 flex flex-col items-center">
                <img src={quantity2} alt="" className="w-20 h-20" />
                <span
                    ref={ref}
                    className="text-[#41b294] text-[50px] font-extrabold"
                >
                    {
                        <CountUp
                            end={33200}
                            duration={5}
                            formattingFn={formatNumber}
                        />
                    }
                </span>
                <span className="font-medium">Học viên</span>
            </div>
            <div className="basis-1/4 md:basis-1/2 p-6 flex flex-col items-center">
                <img src={quantity1} alt="" className="w-20 h-20" />
                <span
                    ref={ref}
                    className="text-[#41b294] text-[50px] font-extrabold"
                >
                    {
                        <CountUp
                            end={400}
                            duration={5}
                            formattingFn={formatNumber}
                        />
                    }
                </span>
                <span className="font-medium">Học viên</span>
            </div>
        </div>
    );
};

export default Quantity;
