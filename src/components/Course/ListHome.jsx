import React, { useEffect, useState } from 'react';
import { courseServ } from '../../services/courseServ';
import { useDispatch, useSelector } from 'react-redux';
import { addCourses } from '../../app/CourseReducer';
import Item from './Item';
import ItemHome from './ItemHome';

const ListHome = () => {
    const [courseList, setCourseList] = useState([]);
    let dispatch = useDispatch();

    useEffect(() => {
        courseServ
            .getCoursesList()
            .then((res) => {
                dispatch(addCourses(res.data));
                setCourseList(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div className="ListHome px-[50px]">
            <h2 className="font-semibold mb-10 text-[#f6ba35] cursor-pointer">
                Khóa học phổ biến
            </h2>
            <div className="grid grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-5">
                {courseList.slice(0, 4)?.map((item, index) => {
                    return <Item course={item} key={index}></Item>;
                })}
            </div>
            <h2 className="font-semibold mb-10 mt-10 cursor-pointer">
                Khóa học tham khảo
            </h2>
            <div className="grid grid-cols-4 gap-5 md:grid-cols-2 sm:grid-cols-1">
                {courseList.slice(4, 8)?.map((item, index) => {
                    return <ItemHome course={item} key={index}></ItemHome>;
                })}
            </div>
            <h2 className="font-semibold mb-10 mt-10 cursor-pointer">
                Khóa học Front End React Js
            </h2>
            <div className="grid grid-cols-4 gap-5 md:grid-cols-2 sm:grid-cols-1">
                {courseList.slice(8, 12)?.map((item, index) => {
                    return <ItemHome course={item} key={index}></ItemHome>;
                })}
            </div>
        </div>
    );
};

export default ListHome;
