import React from 'react';
import avatar2 from '../../assets/img/avatar2.png';
import emoji from '../../assets/img/emoji.png';
import { useRef } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import imgBackup from '../../assets/img/imgBackup.jpg';
import { NavLink } from 'react-router-dom';

const ItemHome = ({ course }) => {
    const divRef = useRef(null);
    const [right_, setRight] = useState(0);
    const [flagRerender, setFlagRerender] = useState(false);

    useEffect(() => {
        const divElement = divRef.current;
        const { right } = divElement.getBoundingClientRect();
        setRight(right);
    }, []);

    return (
        <NavLink to={`/chitiet/${course.maKhoaHoc}`}>
            <div className="cardGlobal mx-auto duration-300 cursor-pointer">
                <div className="relative mb-4">
                    <span className="cardSale absolute z-10 text-white bg-[red] text-[13px] font-semibold px-2 py-[5px]">
                        YÊU THÍCH
                    </span>
                    <img
                        src={course.hinhAnh}
                        onError={(e) => {
                            e.target.src = imgBackup;
                            setFlagRerender(!flagRerender);
                        }}
                        alt=""
                        className="w-full h-[185px]"
                    />
                    <span className="bg-[#41b294] text-white w-fit px-3 absolute bottom-0 translate-y-1/2">
                        {course.tenKhoaHoc.length > 30
                            ? course.tenKhoaHoc.slice(0, 30) + '...'
                            : course.tenKhoaHoc}
                    </span>
                </div>
                <div className="cardBody px-5 pb-5 border-b border-zinc-200">
                    <p
                        className={`font-medium ${
                            course.moTa.length < 30 ? 'mb-8' : ''
                        }`}
                    >
                        {course.moTa.length > 50
                            ? course.moTa.slice(0, 50) + '...'
                            : course.moTa}
                    </p>
                    <div className="flex justify-between items-center mt-2.5 text-[#8c8c8c] mb-4">
                        <div className="flex space-x-2 items-center">
                            <i className="fa-regular fa-clock text-[#f5c002]"></i>
                            <span>8 giờ</span>
                        </div>
                        <div className="flex space-x-2 items-center">
                            <i className="fa-regular fa-calendar-days text-[#f06f68]"></i>
                            <span>4 tuần</span>
                        </div>
                        <div className="flex space-x-2 items-center">
                            <i className="fa-solid fa-signal text-[#65c9ff]"></i>
                            <span>Tất cả</span>
                        </div>
                    </div>
                </div>
                <div className="cardFooter py-[5px] px-5 flex justify-between items-center">
                    <div className="author flex items-center gap-2 mt-2.5">
                        <div className="imgCardBody">
                            <img src={avatar2} alt="" />
                        </div>
                        <span className="text-[#8c8c8c]">Elon Musk</span>
                    </div>
                    <div className="flex flex-col">
                        <div className="line-through text-[#8c8c8c] text-[12px]">
                            800.000 <span>đ</span>
                        </div>
                        <div className="text-[#41b294] font-medium">
                            400.000 <span>đ</span>
                            <i className="fa-solid fa-tag text-[red] text-xl ml-1"></i>
                        </div>
                    </div>
                </div>
                <div
                    ref={divRef}
                    className={`subCard ${
                        right_ > window.innerWidth
                            ? 'left-[-132%] subCardOverflow'
                            : ''
                    }`}
                    onClick={() => {
                        console.log(right_);
                    }}
                >
                    <div className="subCardHead flex items-center">
                        <img
                            src={emoji}
                            alt=""
                            className="w-[50px] h-[50px] object-cover rounded-[50%]"
                        />
                        <span className="ml-2 text-[#8c8c8c]">
                            Elun Musk Ricard
                        </span>
                    </div>
                    <h6 className="font-semibold my-5">
                        BOOTCAMP - LẬP TRÌNH FULL STACK TỪ ZERO ĐẾN CÓ VIỆC
                    </h6>
                    <p className="text-[#8c8c8c]">
                        Đã có hơn 6200 bạn đăng kí học và có việc làm thông qua
                        chương trình đào tạo Bootcamp Lập trình Front End chuyên
                        nghiệp. Khóa học 100% thực hành cường độ cao theo dự án
                        thực tế và kết nối doanh nghiệp hỗ trợ tìm việc ngay sau
                        khi học...
                    </p>
                    <div className="flex justify-between items-center text-[#8c8c8c] mb-3 mt-6">
                        <div className="flex space-x-2 items-center">
                            <i className="fa-regular fa-clock text-[#f5c002]"></i>
                            <span>8 giờ</span>
                        </div>
                        <div className="flex space-x-2 items-center">
                            <i className="fa-regular fa-calendar-days text-[#f06f68]"></i>
                            <span>4 tuần</span>
                        </div>
                        <div className="flex space-x-2 items-center">
                            <i className="fa-solid fa-signal text-[#65c9ff]"></i>
                            <span>Tất cả</span>
                        </div>
                    </div>
                    <button className="p-[15px] bg-[#41b294] text-white hover:scale-95 duration-500">
                        XEM CHI TIẾT
                    </button>
                </div>
            </div>
        </NavLink>
    );
};

export default ItemHome;
