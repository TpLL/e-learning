import React, { useState } from 'react';
import './courseStyle.css';
import avatar2 from '../../assets/img/avatar2.png';
import { NavLink } from 'react-router-dom';
import imgBackup from '../../assets/img/imgBackup.jpg';

const Item = ({ course }) => {
    const [flagRerender, setFlagRerender] = useState(false);

    return (
        <NavLink to={`/chitiet/${course.maKhoaHoc}`}>
            <div className="cardGlobal mx-auto hover:-translate-y-1 duration-300 cursor-pointer">
                <div className="relative mb-4">
                    <img
                        src={course.hinhAnh}
                        onError={(e) => {
                            e.target.src = imgBackup;
                            setFlagRerender(!flagRerender);
                        }}
                        alt=""
                        className="w-full h-[185px]"
                    />
                    <span className="bg-[#41b294] text-white w-fit px-3 absolute bottom-0 translate-y-1/2">
                        {course.tenKhoaHoc.length > 30
                            ? course.tenKhoaHoc.slice(0, 30) + '...'
                            : course.tenKhoaHoc}
                    </span>
                </div>
                <div className="cardBody px-5 pb-5 border-b border-zinc-200">
                    <p
                        className={`font-medium ${
                            course.moTa.length < 30 ? 'mb-8' : ''
                        }`}
                    >
                        {course.moTa.length > 50
                            ? course.moTa.slice(0, 50) + '...'
                            : course.moTa}
                    </p>
                    <div className="author flex items-center gap-2 mt-2.5">
                        <div className="imgCardBody">
                            <img src={avatar2} alt="" />
                        </div>
                        <span className="text-[#8c8c8c]">Elon Musk</span>
                    </div>
                </div>
                <div className="cardFooter py-[5px] px-5 flex justify-between items-center">
                    <div className="flex flex-col justify-between">
                        <div className="line-through text-[#8c8c8c] text-[12px]">
                            800.000 <span>đ</span>
                        </div>
                        <div className="text-[#41b294] font-medium">
                            400.000 <span>đ</span>
                        </div>
                    </div>
                    <div className="flex items-center">
                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                        <span className="text-[#f6ba35] ml-1">4.9</span>
                        <span className="text-[#8c8c8c] text-[12px]">
                            (7840)
                        </span>
                    </div>
                </div>
            </div>
        </NavLink>
    );
};

export default Item;
