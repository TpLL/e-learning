import React from 'react';
import avatarReview from '../../assets/img/avatarReview.png';
import './ReviewStyle.css';

const Review = () => {
    return (
        <div className="reviewStudent mt-12 px-[50px] py-5">
            <div className="py-[50px] flex sm:flex-col relative">
                <div className="triangleTopRight"></div>
                <div className="smallBox smallboxLeftTop"></div>
                <div className="smallBox smallboxRightTop"></div>
                <div className="smallBox smallboxLeftBottom"></div>

                <div className="flex-1 flex justify-center items-center">
                    <div className="bg-avatar flex items-center">
                        <img
                            src={avatarReview}
                            alt=""
                            className="w-[200px] h-[200px] mx-auto"
                        />
                    </div>
                </div>

                <div className="flex-1">
                    <div className="relative">
                        <i className="fa-solid fa-quote-left text-[#ed85ab] text-xl absolute left-[-30px] top-[-12px]"></i>
                        <p className="textQoute text-[17px] leading-[30px]">
                            Chương trình giảng dạy được biên soạn dành riêng cho
                            các bạn Lập trình từ trái ngành hoặc đã có kiến thức
                            theo cường độ cao, luôn được tinh chỉnh và tối ưu
                            hóa theo thời gian bởi các thành viên sáng lập và
                            giảng viên dày kinh nghiệm.Thực sự rất hay và hấp
                            dẫn
                            <i className="fa-solid fa-quote-right text-xs -translate-y-2"></i>
                        </p>
                    </div>
                    <span className="block mt-3 text-[#ed85ab] sm:text-xs">
                        Nhi Dev
                    </span>
                    <span className="block text-[#8c8c8c] sm:text-xs">
                        Học viên xuất sắc
                    </span>
                </div>
            </div>
        </div>
    );
};

export default Review;
