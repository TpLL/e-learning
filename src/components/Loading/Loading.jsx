import React from 'react';
import Lottie from 'lottie-react';
import paperplane from '../../assets/lottie/paperplane.json';
import { useSelector } from 'react-redux';

const Loading = () => {
    const { isLoading } = useSelector((state) => state.loading);
    return (
        isLoading && (
            <div className="fixed inset-0 z-50 bg-[#000000ad] flex items-center justify-center">
                <Lottie
                    style={{ height: 300 }}
                    animationData={paperplane}
                    loop={true}
                />
            </div>
        )
    );
};

export default Loading;
