import React from 'react';
import './BannerStyle.css';
import bannerImg from '../../assets/img/banner.png';
import paperPlane from '../../assets/img/paperPlane.png';
import code_slider from '../../assets/img/code_slider.png';
import message_slider from '../../assets/img/message_slider.png';
import clouds from '../../assets/img/clouds.png';

const Banner = () => {
    return (
        <div className="banner mt-24 px-[50px]">
            <div className="banner_content flex md:flex-col-reverse">
                <div className="flex-1 banner_desc flex items-center justify-center relative">
                    <div className="triangleTopRight"></div>
                    <div className="smallBox smallboxLeftTop"></div>
                    <div className="smallBox smallboxRightTop"></div>
                    <div className="smallBox smallboxRightBottom"></div>
                    <div className="smallBox smallboxRightBottom doubleBox"></div>
                    <div>
                        <div>
                            <img
                                src={paperPlane}
                                alt=""
                                className="paperPlane"
                            />
                        </div>
                        <h1 className="text-[50px] md:text-xl leading-[45px]">
                            Chào mừng
                        </h1>
                        <h1 className="text-[50px] md:text-xl">
                            đến với môi trường
                        </h1>
                        <h1>
                            V<span className="ml-1 text-[50px]">learning</span>
                        </h1>
                        <button className="btnBanner px-5 py-2.5 md:p-[5px] hover:scale-95 duration-300">
                            BẮT ĐẦU NÀO
                        </button>
                    </div>
                </div>
                <div className="flex-1 banner_img_block relative">
                    <div>
                        <img src={bannerImg} alt="" />
                        <img
                            className="sliderSubImg sliderCodeImg"
                            src={code_slider}
                            alt=""
                        ></img>
                        <img
                            className="sliderSubImg sliderMesImg "
                            src={message_slider}
                            alt=""
                        ></img>
                        <img
                            className="sliderSubImg sliderCloudImg"
                            src={clouds}
                            alt=""
                        ></img>
                        <img
                            className="sliderSubImg sliderCloud2Img"
                            src={clouds}
                            alt=""
                        ></img>
                        <img
                            className="sliderSubImg sliderCloud3Img"
                            src={clouds}
                            alt=""
                        ></img>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Banner;
