import React from 'react';

const BannerGlobal = ({ title, subTitle }) => {
    return (
        <div className="courses_banner p-[50px] bg-[#ffd60a] text-white">
            <h2 className="text-[28px] sm:text-[20px] font-medium">{title}</h2>
            <p className="text-[13px] sm:text-[10px]">{subTitle}</p>
        </div>
    );
};

export default BannerGlobal;
