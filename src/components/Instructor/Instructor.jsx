import React from 'react';
import Slider from 'react-slick';
import './InstructorStyle.css';
import instrutor from '../../assets/img/instrutor.jpg';
import instrutor2 from '../../assets/img/instrutor2.jpg';
import instrutor3 from '../../assets/img/instrutor3.jpg';
import instrutor4 from '../../assets/img/instrutor4.jpg';
import instrutor5 from '../../assets/img/instrutor5.jpg';
import instrutor6 from '../../assets/img/instrutor6.jpg';
import instrutor7 from '../../assets/img/instrutor7.jpg';

const Instructor = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 6,
        slidesToScroll: 6,
        autoPlay: false,
        customPaging: (i) => (
            <div
                style={{
                    width: '12px',
                    height: '12px',
                    background: '#41b294',
                    borderRadius: '999px',
                    opacity: '0.5',
                }}
            ></div>
        ),
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                },
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    };

    return (
        <div className="px-[50px]">
            <h2 className="cursor-pointer inline-block font-medium">
                {' '}
                Giảng viên hàng đầu{' '}
            </h2>
            <Slider {...settings} className="text-center">
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">IcarDi MenBor</p>
                        <p className="mt-2 mb-1">Chuyên gia ngôn ngữ Vue Js</p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor2}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">Bladin Slaham</p>
                        <p className="mt-2 mb-1">
                            Chuyên gia hệ thống máy tính
                        </p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor3}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">Chris Andersan</p>
                        <p className="mt-2 mb-1">
                            Chuyên gia lĩnh vực Full Skill
                        </p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor4}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">VueLo Gadi</p>
                        <p className="mt-2 mb-1">
                            Chuyên gia lĩnh vực Phân tích
                        </p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor5}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">Hoàng Nam</p>
                        <p className="mt-2 mb-1">Chuyên gia lĩnh vực PHP</p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor6}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">IcarDi MenBor</p>
                        <p className="mt-2 mb-1">Chuyên gia lĩnh vực PHP</p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
                <div className="px-[15px]">
                    <div className="instructorItem p-[15px] my-5 duration-300">
                        <img
                            src={instrutor7}
                            alt=""
                            className="w-20 h-20 object-cover rounded-[50%] mx-auto"
                        />
                        <p className="font-medium">IcarDi MenBor</p>
                        <p className="mt-2 mb-1">Chuyên gia ngôn ngữ Vue Js</p>
                        <div className="text-[#f6ba35]">
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <i className="fa-solid fa-star"></i>
                            <span className="ml-1">4.9</span>
                        </div>
                        <span className="text-[#8c8c8c] text-[13px]">
                            100 Đánh giá
                        </span>
                    </div>
                </div>
            </Slider>
        </div>
    );
};

export default Instructor;
