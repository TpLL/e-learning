import React from 'react';
import './suKien.css';

export default function DesktopSuKien() {
    return (
        <div>
            <div className="mt-24">
                <div className="bannerEvent">
                    <div className="timeBannerEvent text-white">
                        <div className="displayTime flex space-x-5">
                            <div className="timeEvent">
                                <p className=" text-yellow-500">00</p>
                                <p className="text-center">
                                    <small className="text-2xl">Ngày</small>
                                </p>
                            </div>
                            <div className="timeEvent">
                                <p className=" text-yellow-700">00</p>
                                <p className="text-center">
                                    <small className="text-2xl">Giờ</small>
                                </p>
                            </div>
                            <div className="timeEvent">
                                <p className=" text-pink-600">00</p>
                                <p className="text-center">
                                    <small className="text-2xl">Phút</small>
                                </p>
                            </div>
                            <div className="timeEvent">
                                <p className=" text-purple-500">00</p>
                                <p className="text-center">
                                    <small className="text-2xl">Giây</small>
                                </p>
                            </div>
                        </div>
                        <h4 className="text-4xl font-bold my-3">
                            Sự kiện công nghệ lớn nhất 2021
                        </h4>
                        <h6 className="text-xl font-bold">
                            20 - 25 tháng 12, 2022, Việt Nam
                        </h6>
                    </div>
                </div>

                <div className="eventDetail p-5">
                    <div className=" grid grid-cols-2">
                        <div className="imgEvent">
                            <img
                                alt=""
                                className="animate-[bounce_3s_linear_infinite]"
                                src="https://demo2.cybersoft.edu.vn/static/media/it.ef68b551.png"
                            ></img>
                        </div>
                        <div className="inforEvent">
                            <h5>Sự kiện công nghệ dành cho startup</h5>
                            <h6 className="font-bold">
                                Nơi gặp gỡ của những tư tưởng lớn
                            </h6>
                            <p className="text-gray-400 py-3">
                                Innovatube Frontier Summit (IFS) là sự kiện đầu
                                tiên tại Việt Nam tập trung vào cả bốn mảng tiêu
                                biểu của công nghệ tiên phong, bao gồm
                                Artificial Intelligence (trí tuệ nhân tạo),
                                Internet of Things (Internet vạn vật),
                                Blockchain (Chuỗi khối) và Augmented
                                reality/Virtual Reality (Thực tế tăng cường/Thực
                                tế ảo)
                            </p>
                            <button className="bg-green-600">THAM GIA</button>
                            <button className="bg-yellow-500 ml-2">
                                TÌM HIỂU THÊM
                            </button>
                        </div>
                    </div>
                </div>

                <div className="speecher">
                    <h6>Các nhà đồng sáng tạo</h6>
                    <div className="detailSpeecher grid grid-cols-4 gap-10 p-12 text-white uppercase">
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor5.2e4bd1e6.jpg"
                                ></img>
                                <h6>Nguyễn Nhật</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor6.64041dca.jpg"
                                ></img>
                                <h6>Nguyễn Nhật Nam</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor7.edd00a03.jpg"
                                ></img>
                                <h6>Nguyễn Nam</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor8.aec2f526.jpg"
                                ></img>
                                <h6>Jhonny Đặng</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor9.504ea6c5.jpg"
                                ></img>
                                <h6>Ngô Henry</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor10.89946c43.jpg"
                                ></img>
                                <h6>Vương Phạm Vn</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor11.0387fe65.jpg"
                                ></img>
                                <h6>Khoa Pug</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                        <div className="itemSpeecher">
                            <div className="containSpeecher">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/instrutor12.90a80820.jpg"
                                ></img>
                                <h6>Nguyễn Nhật</h6>
                                <p>Ceo TechViet Production</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="donors my-24">
                    <h6>Nhà tài trợ chương trình</h6>
                    <div className="containDonors grid grid-cols-4 space-x-5 p-10">
                        <div className="itemDonors pl-3">
                            <div className="contentDonors">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/meta.10fa2fa1.jpg"
                                ></img>
                                <p>Facebook</p>
                            </div>
                        </div>
                        <div className="itemDonors">
                            <div className="contentDonors">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/microsoft.318b3280.jpg"
                                ></img>
                                <p>Microsoft</p>
                            </div>
                        </div>
                        <div className="itemDonors">
                            <div className="contentDonors">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/Google-logo.f11902b5.jpg"
                                ></img>
                                <p>Google</p>
                            </div>
                        </div>
                        <div className="itemDonors">
                            <div className="contentDonors">
                                <img
                                    alt=""
                                    src="https://demo2.cybersoft.edu.vn/static/media/amazon.996890c4.jpg"
                                ></img>
                                <p>Amazon</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
