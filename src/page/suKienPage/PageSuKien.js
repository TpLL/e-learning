import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopSuKien from "./DesktopSuKien";
import TableSuKien from "./TableSuKien";
import MobileSuKien from "./MobileSuKien";

export default function PageSuKien() {
  return (
    <div>
      <Desktop>
        <DesktopSuKien />
      </Desktop>
      <Tablet>
        <TableSuKien />
      </Tablet>
      <Mobile>
        <MobileSuKien />
      </Mobile>
    </div>
  );
}
