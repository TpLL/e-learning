import React from 'react';
import { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { courseServ } from '../../services/courseServ';
import { useState } from 'react';
import ItemHome from '../../components/Course/ItemHome';
import BannerGlobal from '../../components/Banner/BannerGlobal';

const CategoryCourses = () => {
    const { cate } = useParams();
    const [courses, setCourses] = useState();

    useEffect(() => {
        courseServ
            .getCoursesByCate(cate)
            .then((res) => {
                setCourses(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [cate]);

    return (
        courses && (
            <div className="mt-24">
                <BannerGlobal
                    title="KHÓA HỌC THEO DANH MỤC"
                    subTitle="HÃY CHỌN KHÓA HỌC MONG MUỐN !!!"
                ></BannerGlobal>

                <div className="p-[50px]">
                    <h1 className="border-2 border-[#e4e4e4] rounded-[25px] inline-block px-2.5 py-3 font-medium">
                        <i className="fa-solid fa-display mr-2 text-[#f6ba35]"></i>
                        {courses[0].danhMucKhoaHoc.tenDanhMucKhoaHoc}
                    </h1>
                    <div className="mt-10 grid grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-8">
                        {courses.map((item, index) => {
                            return (
                                <ItemHome course={item} key={index}></ItemHome>
                            );
                        })}
                    </div>
                </div>
            </div>
        )
    );
};

export default CategoryCourses;
