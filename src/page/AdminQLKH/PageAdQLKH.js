import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopAdQLKH from "./DesktopAdQLKH";
import TableAdQLKH from "./TableAdQLKH";
import MobileAdQLKH from "./MobileAdQLKH";

export default function PageAdQLKH() {
  return (
    <div>
      <Desktop>
        <DesktopAdQLKH />
      </Desktop>
      <Tablet>
        <TableAdQLKH />
      </Tablet>
      <Mobile>
        <MobileAdQLKH />
      </Mobile>
    </div>
  );
}
