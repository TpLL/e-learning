import React, { useEffect } from 'react';
import './adminQLKH.css';
import { Table, Button, Modal, Input } from 'antd';
import { Dropdown, Space } from 'antd';
import { useState } from 'react';
import { courseAPI } from '../../services/QuanLyAPI/severAPI';
import { useSelector } from 'react-redux';
import AddCourse from '../../feature/FeatureCourse/AddCourse';
import UpdatePageQLKH from '../../feature/FeatureCourse/UpdatePageQLKH';
import FixCourse from '../../feature/FeatureCourse/FixCourse';
import DeleteCourse from '../../feature/FeatureCourse/DeleteCourse';
import RegisterPageCourse from '../../feature/FeatureCourse/RegisterPageCourse';
import { userLocal } from '../../services/QuanLyAPI/localUser';
import { useNavigate } from 'react-router-dom';

export default function DesktopAdQLKH() {
    const navigate = useNavigate();
    const [dataListCourse, setDataListCourse] = useState();
    const [valueFindCourse, setValueFindCourse] = useState();
    const listCourse = useSelector(
        (state) => state.elearnningReducer.listCourse
    );
    const valueUserLogin = useSelector(
        (state) => state.elearnningReducer.valueUserLogin
    );
    const numberRandom = Math.floor(Math.random() * 10);
    const imgRandom = `https://i.pravatar.cc/150?img=${numberRandom}`;
    useEffect(() => {
        if(userLocal.get()){
            courseAPI
            .getListCourse()
            .then((res) => {
                setDataListCourse(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }else{
        navigate("/login")
    }
        }, [listCourse]
        );

    const renderDataListCourse = (data) => {
        return data?.map((course, index) => {
            return {
                stt: index + 1,
                maKhoaHoc: course.maKhoaHoc,
                tenKhoaHoc: course.tenKhoaHoc,
                hinhAnh: (
                    <img
                        style={{
                            height: 40,
                            width: '100%',
                            objectFit: 'cover',
                            objectPosition: 'center',
                        }}
                        src={course.hinhAnh}
                    ></img>
                ),
                luotXem: course.luotXem,
                nguoiTao: course.nguoiTao.hoTen,
                action: (
                    <div className="space-x-2">
                        <button>
                            <RegisterPageCourse course={course} />
                        </button>
                        <button className="border bg-yellow-500 rounded">
                            <FixCourse course={course} />
                        </button>
                        <button className="border bg-red-600 rounded">
                            <DeleteCourse course={course} />
                        </button>
                    </div>
                ),
            };
        });
    };

    const renderHeader = () => {
        if (userLocal.get()) {
            return (
                <>
                    <h3>Chào {valueUserLogin.hoTen} , </h3>
                    <img style={{ height: 30 }} src={imgRandom}></img>
                    <div>
                        <Space direction="vertical">
                            <Dropdown
                                menu={{
                                    items,
                                }}
                                placement="bottomLeft"
                            >
                                <Button>
                                    <i class="fa fa-arrow-down"></i>
                                </Button>
                            </Dropdown>
                        </Space>
                    </div>
                </>
            );
        } else {
            return (
                <a href="/login">
                    <button className=" px-3 py-2 rounded bg-white font-bold  ">
                        ĐĂNG NHẬP
                    </button>
                </a>
            );
        }
    };

    const handleLogout = () => {
        return userLocal.remove();
    };

    // INPUT ANTD
    const onChange = (e) => {
        console.log(e.target.value);
        let value = e.target.value;
        if (value != '') {
            return courseAPI
                .getListCoursePathPage(e.target.value)
                .then((res) => {
                    console.log(res.data.items);
                    setValueFindCourse(res.data.items);
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            return setValueFindCourse('');
        }
    };

    // MODAL ANTD
    const [isModalOpenAddCourse, setIsModalOpenAddCourse] = useState(false);

    const showModalAddCourse = () => {
        setIsModalOpenAddCourse(true);
    };
    const handleOkAddCourse = () => {
        setIsModalOpenAddCourse(true);
    };
    const handleCancelAddCourse = () => {
        setIsModalOpenAddCourse(false);
    };

    // DATA TABLE ANTD
    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            align: 'center',
        },
        {
            title: 'Mã khoá học',
            dataIndex: 'maKhoaHoc',
            align: 'center',
        },
        {
            title: 'Hình ảnh',
            dataIndex: 'hinhAnh',
            align: 'center',
        },
        {
            title: 'Tên khoá học',
            dataIndex: 'tenKhoaHoc',
            align: 'center',
        },

        {
            title: 'Lượt xem',
            dataIndex: 'luotXem',
            align: 'center',
        },
        {
            title: 'Người tạo',
            dataIndex: 'nguoiTao',
            align: 'center',
        },
        {
            title: 'Action',
            dataIndex: 'action',
            align: 'center',
        },
    ];
    const data = () => {
        if (
            valueFindCourse == null ||
            valueFindCourse == '' ||
            valueFindCourse == undefined
        ) {
            return renderDataListCourse(dataListCourse);
        } else {
            return renderDataListCourse(valueFindCourse);
        }
    };

    // DROPDOWN ANTD
    const items = [
        {
            key: '1',
            label: <UpdatePageQLKH />,
        },
        {
            key: '2',
            label: (
                <a
                    rel="noopener noreferrer"
                    href="/login"
                    onClick={handleLogout}
                >
                    Đăng xuất
                </a>
            ),
        },
    ];

    return (
        <div className="bg_all flex">
            <div className="w-1/12 bg-black/20 m-3 flex flex-col space-y-10 items-center p-3 text-center rounded-xl">
                <div className="iconHome">
                    <a href="/">
                        <i className="fa fa-home text-black bg-none text-3xl bg-white px-3 py-3 rounded hover:text-green-400 duration-500"></i>
                    </a>
                </div>
                <div className="user hover:bg-white duration-500 hover:rounded-xl">
                    <a
                        href="/admin/quanlynguoidung"
                        className="hover:no-underline hover:text-green-400 duration-500 block px-3 py-3"
                    >
                        <i className="fa fa-user mr-2"></i>Quản lý người dùng
                    </a>
                </div>
                <div className="course px5 py-4 hover:bg-white duration-500 hover:rounded-xl">
                    <a
                        href="/admin/quanlykhoahoc"
                        className="hover:no-underline hover:text-green-400 duration-500  block px-3 py-3"
                    >
                        <i className="fa fa-briefcase mr-1"></i>Quản lý khóa học
                    </a>
                </div>
            </div>
            <div className="w-11/12 p-3 space-y-5">
                <div className="flex justify-between">
                    <Button
                        type="primary"
                        style={{
                            backgroundColor: 'green',
                            padding: 20,
                            lineHeight: 0.1,
                        }}
                        onClick={showModalAddCourse}
                    >
                        Thêm khoá học
                    </Button>

                    <Input
                        placeholder="Nhập vào khoá học cần tìm"
                        style={{ width: '40%' }}
                        onChange={onChange}
                    />

                    <Modal
                        width={900}
                        open={isModalOpenAddCourse}
                        onOk={handleOkAddCourse}
                        onCancel={handleCancelAddCourse}
                        footer={null}
                    >
                        <AddCourse />
                    </Modal>

                    <div className="flex items-center space-x-2">
                        {renderHeader()}
                    </div>
                </div>
                <div className="">
                    <Table
                        columns={columns}
                        dataSource={data()}
                        size="middle"
                        bordered
                    />
                </div>
            </div>
        </div>
    );
}
