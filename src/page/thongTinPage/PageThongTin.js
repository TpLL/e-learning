import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopThongTinPage from "./DesktopThongTinPage";
import TableThongTinPage from "./TableThongTinPage";
import MobileThongTinPage from "./MobileThongTinPage";

export default function PageThongTin() {
  return (
    <div>
      <Desktop>
        <DesktopThongTinPage />
      </Desktop>
      <Tablet>
        <TableThongTinPage />
      </Tablet>
      <Mobile>
        <MobileThongTinPage />
      </Mobile>
    </div>
  );
}
