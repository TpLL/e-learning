import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopLogin from "./DesktopLogin";
import TableLogin from "./TableLogin";
import MobileLogin from "./MobileLogin";

export default function PageLogin() {
  return (
    <div>
      <Desktop>
        <DesktopLogin />
      </Desktop>
      <Tablet>
        <TableLogin />
      </Tablet>
      <Mobile>
        <MobileLogin />
      </Mobile>
    </div>
  );
}
