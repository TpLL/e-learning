import React from 'react';
import './login.css';
import { Button, Form, Input, Select, message } from 'antd';
import { useDispatch } from 'react-redux';
import { userLogin } from '../../app/elearningReducer';
import { userAPI } from '../../services/QuanLyAPI/severAPI';

//  START constant ANTD SIGN IN

const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
};

//END constant ANTD LOGIN

export default function DesktopLogin() {
    const dispatch = useDispatch();
    const onFinishLogin = (values) => {
        const actionThunk = userLogin(values);
        dispatch(actionThunk);
    };

    const onFinishSignIn = (values) => {
        userAPI
            .userSignInApi(values)
            .then((res) => {
                message.success('Đăng ký thành công !');
            })
            .catch((err) => {
                message.error(`${err.response.data}`);
            });
    };

    return (
        <div className="bgFull w-screen h-screen container-fluid flex justify-center items-center">
            {/* dang nhap */}

            <div className="bgForm flex">
                <div className="basis-1/2 m-auto p-10">
                    <div className="">
                        <Form
                            name="basic"
                            labelCol={{
                                span: 8,
                            }}
                            wrapperCol={{
                                span: 24,
                            }}
                            style={{
                                maxWidth: 600,
                            }}
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinishLogin}
                            onFinishFailed={onFinishFailed}
                            autoComplete="off"
                        >
                            <div className="text-center text-3xl">
                                <h1 className="font-bold">Đăng nhập</h1>
                                <span>
                                    hoặc sử dụng tài khoản đã đăng ký của bạn
                                </span>
                            </div>
                            <Form.Item
                                name="taiKhoan"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập Tài khoản!',
                                    },
                                ]}
                            >
                                <Input
                                    className=""
                                    placeholder="Tài khoản"
                                    classNames=""
                                />
                            </Form.Item>

                            <Form.Item
                                name="matKhau"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mật khẩu!',
                                    },
                                ]}
                            >
                                <Input.Password
                                    className=""
                                    placeholder="Mật khẩu"
                                />
                            </Form.Item>

                            <Form.Item
                            // wrapperCol={{
                            //   offset: 8,
                            //   span: 16,
                            // }}
                            >
                                <div className="flex justify-center">
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        className="buttonLogin"
                                    >
                                        <div className="text-xl">Đăng nhập</div>
                                    </Button>
                                </div>
                            </Form.Item>
                        </Form>
                    </div>
                </div>

                {/* dang ky */}

                <div className="basis-1/2 m-auto p-10 overflow-scroll">
                    <div className="container">
                        <Form
                            name="nest-messages"
                            onFinish={onFinishSignIn}
                            style={{
                                maxWidth: 500,
                            }}
                            // validateMessages={validateMessages}
                        >
                            <div className="text-center text-3xl">
                                <h1 className="font-bold ">Đăng ký</h1>
                                <span className="">
                                    Nếu bạn chưa có tài khoản sử dụng
                                </span>
                            </div>
                            <Form.Item
                                error="Tài khoản"
                                name="taiKhoan"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập Tài khoản!',
                                    },
                                ]}
                            >
                                <Input
                                    placeholder="Tài khoản"
                                    className=""
                                    // style={{ padding: 15, backgroundColor: "rgb(196, 192, 192)" }}
                                />
                            </Form.Item>
                            <Form.Item
                                name="hoTen"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập Họ tên!',
                                    },
                                ]}
                            >
                                <Input placeholder="Họ tên" className="" />
                            </Form.Item>
                            <Form.Item
                                name="matKhau"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập Mật khẩu!',
                                    },
                                ]}
                            >
                                <Input
                                    type="password"
                                    placeholder="Mật khẩu"
                                    className=""
                                />
                            </Form.Item>
                            <Form.Item
                                name="email"
                                rules={[
                                    {
                                        type: 'email',
                                        required: true,
                                        message: 'Vui lòng nhập Email!',
                                    },
                                ]}
                            >
                                <Input placeholder="Email" className="" />
                            </Form.Item>
                            <Form.Item
                                name="soDT"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập Số điện thoại!',
                                    },
                                ]}
                            >
                                <Input
                                    placeholder="Số điện thoại"
                                    className=""
                                />
                            </Form.Item>
                            <Form.Item
                                name="maNhom"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mã Nhóm!',
                                    },
                                ]}
                            >
                                <Select className="" placeholder="Mã nhóm">
                                    <Select.Option value="GP01">
                                        GP01
                                    </Select.Option>
                                    <Select.Option value="GP02">
                                        GP02
                                    </Select.Option>
                                    <Select.Option value="GP03">
                                        GP03
                                    </Select.Option>
                                    <Select.Option value="GP04">
                                        GP04
                                    </Select.Option>
                                    <Select.Option value="GP05">
                                        GP05
                                    </Select.Option>
                                </Select>
                            </Form.Item>

                            <Form.Item
                            // wrapperCol={{
                            //   ...layout.wrapperCol,
                            //   offset: 8,
                            // }}
                            >
                                <div className="flex justify-center">
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        className="buttonLogin"
                                    >
                                        <span className="text-xl">Đăng ký</span>
                                    </Button>
                                </div>
                            </Form.Item>
                        </Form>
                    </div>

                    {/* overlay */}
                    {/* <div className="overlay-container">
            <div className="overlay">
              <div className="overlay-panel overlay-left">
                <h1>Chào mừng bạn đã trở lại!</h1>
                <p>Vui lòng đăng nhập để kết nối với tài khoản của bạn</p>
                <button
                  className="buttonLogin px-8 py-2"
                  // onClick={handleMove()}
                >
                  Đăng nhập
                </button>
              </div>
              <div className="overlay-panel overlay-right">
                <h1>Xin chào!</h1>
                <p>
                  Vui lòng nhấn đăng ký để thiết lập thông tin tài khoản của
                  bạn!
                </p>
                <button
                  className="buttonLogin px-8 py-2"
                  // onClick={handleMove()}
                >
                  Đăng ký
                </button>
              </div>
            </div>
          </div> */}
                </div>
            </div>
        </div>
    );
}
