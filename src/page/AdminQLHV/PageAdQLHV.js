import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopAdQLHV from "./DesktopAdQLHV";
import TableAdQLHV from "./TableAdQLHV";
import MobileAdQLHV from "./MobileAdQLHV";

export default function PageAdQLHV() {
  return (
    <div>
      <Desktop>
        <DesktopAdQLHV />
      </Desktop>
      <Tablet>
        <TableAdQLHV />
      </Tablet>
      <Mobile>
        <MobileAdQLHV />
      </Mobile>
    </div>
  );
}
