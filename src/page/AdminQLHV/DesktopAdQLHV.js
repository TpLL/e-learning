import React, { useEffect } from 'react';
import './adminQLHV.css';
import { Table, Button, Modal, Input } from 'antd';
import { Dropdown, Space } from 'antd';
import { useState } from 'react';
import { userAPI } from '../../services/QuanLyAPI/severAPI';
import { listUserLocal, userLocal } from '../../services/QuanLyAPI/localUser';
import { useSelector } from 'react-redux';
import RegisterCourse from '../../feature/FeatureUser/RegisterCourse';
import DeleteModal from '../../feature/FeatureUser/DeleteModal';
import FixModalUser from '../../feature/FeatureUser/FixModalUser';
import UpdateFormAntd from '../../feature/FeatureUser/UpdateFormAntd';
import AddUserFormAntd from '../../feature/FeatureUser/AddUserFormAntd';
import { useNavigate } from 'react-router-dom';

export default function DesktopAdQLHV() {
    const navigate = useNavigate();
    const [dataInforUser, setDataUserInfor] = useState();
    const [valueFindUser, setValueFindUser] = useState();
    const numberRandom = Math.floor(Math.random() * 10);
    const imgRandom = `https://i.pravatar.cc/150?img=${numberRandom}`;
    const listUser = useSelector((state) => state.elearnningReducer.listUser);
    const valueUserLogin = useSelector(
        (state) => state.elearnningReducer.valueUserLogin
    );
    useEffect(() => {
        if (userLocal.get()) {
            userAPI
                .getListUserApi()
                .then((res) => {
                    console.log(res.data);
                    setDataUserInfor(res.data);
                    listUserLocal.set(res.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        } else {
            navigate('/login');
        }
    }, [listUser]);

    const renderDataUser = (data) => {
        return data?.map((user, index) => {
            return {
                stt: index + 1,
                taiKhoan: user.taiKhoan,
                maLoaiNguoiDung: user.maLoaiNguoiDung,
                hoTen: user.hoTen,
                email: user.email,
                soDt: user.soDt,
                action: (
                    <div className="space-x-2">
                        <button>
                            <RegisterCourse user={user} />
                        </button>
                        <button className="border bg-yellow-500 rounded">
                            <FixModalUser user={user} />
                        </button>
                        <button className="border bg-red-600 rounded">
                            <DeleteModal user={user} />
                        </button>
                    </div>
                ),
            };
        });
    };

    const renderHeader = () => {
        if (userLocal.get()) {
            return (
                <>
                    <h3>Chào {valueUserLogin.hoTen} , </h3>
                    <img style={{ height: 30 }} src={imgRandom}></img>
                    <div>
                        <Space direction="vertical">
                            <Dropdown
                                menu={{
                                    items,
                                }}
                                placement="bottomLeft"
                            >
                                <Button>
                                    <i class="fa fa-arrow-down"></i>
                                </Button>
                            </Dropdown>
                        </Space>
                    </div>
                </>
            );
        } else {
            return (
                <a href="/login">
                    <button className=" px-3 py-2 rounded bg-white font-bold  ">
                        ĐĂNG NHẬP
                    </button>
                </a>
            );
        }
    };

    const handleLogout = () => {
        return userLocal.remove();
    };

    // INPUT ANTD
    const onChange = (e) => {
        console.log(e.target.value);
        userAPI
            .findUser(e.target.value)
            .then((res) => {
                console.log(res.data);
                setValueFindUser(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    // MODAL ANTD
    const [isModalOpenAddUser, setIsModalOpenAddUser] = useState(false);

    const showModalAddUser = () => {
        setIsModalOpenAddUser(true);
    };
    const handleOkAddUser = () => {
        setIsModalOpenAddUser(true);
    };
    const handleCancelAddUser = () => {
        setIsModalOpenAddUser(false);
    };

    // DATA TABLE ANTD
    const columns = [
        {
            title: 'STT',
            dataIndex: 'stt',
            align: 'center',
        },
        {
            title: 'Tài khoản',
            dataIndex: 'taiKhoan',
            align: 'center',
        },
        {
            title: 'Người dùng',
            dataIndex: 'maLoaiNguoiDung',
            align: 'center',
        },
        {
            title: 'Họ tên',
            dataIndex: 'hoTen',
            align: 'center',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            align: 'center',
        },
        {
            title: 'Số ĐT',
            dataIndex: 'soDt',
            align: 'center',
        },
        {
            title: 'Action',
            dataIndex: 'action',
            align: 'center',
        },
    ];
    const data = () => {
        if (
            valueFindUser == null ||
            valueFindUser == '' ||
            valueFindUser == undefined
        ) {
            return renderDataUser(dataInforUser);
        } else {
            return renderDataUser(valueFindUser);
        }
    };

    // DROPDOWN ANTD
    const items = [
        {
            key: '1',
            label: <UpdateFormAntd />,
        },
        {
            key: '2',
            label: (
                <a
                    rel="noopener noreferrer"
                    href="/login"
                    onClick={handleLogout}
                >
                    Đăng xuất
                </a>
            ),
        },
    ];

    return (
        <div className="bg_all flex">
            <div className="w-1/12 bg-black/20 m-3 flex flex-col space-y-10 items-center p-3 text-center rounded-xl">
                <div className="iconHome">
                    <a href="/">
                        <i className="fa fa-home text-black bg-none text-3xl bg-white px-3 py-3 rounded hover:text-green-400 duration-500"></i>
                    </a>
                </div>
                <div className="user hover:bg-white duration-500 hover:rounded-xl">
                    <a
                        href="/admin/quanlynguoidung"
                        className="hover:no-underline hover:text-green-400 duration-500 block px-3 py-3"
                    >
                        <i className="fa fa-user mr-2"></i>Quản lý người dùng
                    </a>
                </div>
                <div className="course px5 py-4 hover:bg-white duration-500 hover:rounded-xl">
                    <a
                        href="/admin/quanlykhoahoc"
                        className="hover:no-underline hover:text-green-400 duration-500  block px-3 py-3"
                    >
                        <i className="fa fa-briefcase mr-1"></i>Quản lý khóa học
                    </a>
                </div>
            </div>
            <div className="w-11/12 p-3 space-y-5">
                <div className="flex justify-between">
                    <Button
                        type="primary"
                        style={{
                            backgroundColor: 'green',
                            padding: 20,
                            lineHeight: 0.1,
                        }}
                        onClick={showModalAddUser}
                    >
                        Thêm người dùng
                    </Button>

                    <Input
                        placeholder="Nhập vào tài khoản hoặc họ tên người dùng"
                        style={{ width: '40%' }}
                        onChange={onChange}
                    />

                    <Modal
                        open={isModalOpenAddUser}
                        onOk={handleOkAddUser}
                        onCancel={handleCancelAddUser}
                        footer={null}
                    >
                        <AddUserFormAntd />
                    </Modal>

                    <div className="flex items-center space-x-2">
                        {renderHeader()}
                    </div>
                </div>
                <div className="">
                    <Table
                        columns={columns}
                        dataSource={data()}
                        size="middle"
                        bordered
                    />
                </div>
            </div>
        </div>
    );
}
