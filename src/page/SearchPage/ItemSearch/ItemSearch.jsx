import React from 'react';
import avarInstructor from '../../../assets/img/instrutor6.jpg';
import { NavLink } from 'react-router-dom';

const ItemSearch = ({ course, handleRemoveCourse }) => {
    return (
        <div className="item_search my-[30px] flex md:flex-col">
            <div className="basis-3/12 px-[15px]">
                <img src={course.hinhAnh} alt="" className="w-full h-52" />
            </div>

            <div className="basis-7/12 px-[15px]">
                <h4 className="font-medium">{course.tenKhoaHoc}</h4>
                <p className="my-[5px] text-[#8c8c8c]">
                    ES6 là một chuẩn Javascript mới được đưa ra vào năm 2015 với
                    nhiều quy tắc và cách sử dụng khác nhau...
                </p>
                <div className="flex items-center gap-5">
                    <div className="flex space-x-2 items-center">
                        <i className="fa-regular fa-clock text-[#f5c002]"></i>
                        <span>8 giờ</span>
                    </div>
                    <div className="flex space-x-2 items-center">
                        <i className="fa-regular fa-calendar-days text-[#f06f68]"></i>
                        <span>4 tuần</span>
                    </div>
                    <div className="flex space-x-2 items-center">
                        <i className="fa-solid fa-signal text-[#65c9ff]"></i>
                        <span>All level</span>
                    </div>
                </div>
                <div className="text-[#f6ba35] my-[5px]">
                    <i className="fa-solid fa-star"></i>
                    <i className="fa-solid fa-star"></i>
                    <i className="fa-solid fa-star"></i>
                    <i className="fa-solid fa-star"></i>
                    <i className="fa-solid fa-star"></i>
                </div>
                <div className="flex items-center gap-2 mb-[5px]">
                    <div>
                        <img
                            src={avarInstructor}
                            alt=""
                            className="w-10 h-10 rounded-full object-cover"
                        />
                    </div>
                    <span>Nguyễn Nam</span>
                </div>
            </div>

            <div className="basis-2/12 flex items-end justify-center md:justify-start md:px-[15px]">
                <NavLink to={`/chitiet/${course.maKhoaHoc}`}>
                    <button className="btn-viewDetail text-white bg-[#f6ba35] py-[5px] px-2.5 hover:scale-95 duration-300">
                        XEM CHI TIẾT
                    </button>
                </NavLink>
                <button
                    className="btn-removeCourse text-white bg-[#f6ba35] py-[5px] px-2.5 hover:scale-95 duration-300"
                    onClick={() => {
                        handleRemoveCourse(course.maKhoaHoc);
                    }}
                >
                    HỦY KHÓA HỌC
                </button>
            </div>
        </div>
    );
};

export default ItemSearch;
