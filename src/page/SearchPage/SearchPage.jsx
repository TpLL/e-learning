import React, { useEffect, useState } from 'react';
import './SearchPageStyle.css';
import { useParams } from 'react-router-dom';
import { courseServ } from '../../services/courseServ';
import ItemSearch from './ItemSearch/ItemSearch';
import { useDispatch } from 'react-redux';
import { addCourses } from '../../app/CourseReducer';
import BannerGlobal from '../../components/Banner/BannerGlobal';
import { offLoading, onLoading } from '../../app/LoadingReducer';

const SearchPage = () => {
    const { keyword } = useParams();
    let dispatch = useDispatch();
    const [coursesBySearch, setCoursesBySearch] = useState();

    useEffect(() => {
        dispatch(onLoading());
        courseServ
            .getCoursesList()
            .then((res) => {
                dispatch(addCourses(res.data));
                setCoursesBySearch(
                    res.data.filter((item) =>
                        item.biDanh
                            .toLowerCase()
                            .includes(keyword.toLowerCase())
                    )
                );
                dispatch(offLoading());
            })
            .catch((err) => {
                console.log(err);
                dispatch(offLoading());
            });
    }, []);

    return (
        <div className="mt-24 searchPage">
            <BannerGlobal
                title="TÌM KIẾM"
                subTitle="KẾT QUẢ TÌM KIẾM KHÓA HỌC!!!"
            ></BannerGlobal>

            <div className="flex sm:justify-center mx-[50px] my-5 gap-[30px]">
                <div className="search_sidebar basis-1/6 md:basis-1/3 sm:hidden">
                    <span className="ml-1 font-medium text-[25px]">Lọc</span>
                    <div className="mb-6">
                        <span className="filter_title block my-2.5 font-medium text-xl">
                            Khóa học
                        </span>
                        <ul className="pl-[15px] space-y-4">
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Tất cả</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Front End</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Back End</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">HTML/CSS</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div className="mb-6">
                        <span className="filter_title block my-2.5 font-medium text-xl">
                            Cấp độ
                        </span>
                        <ul className="pl-[15px] space-y-4">
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Tất cả</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Mới bắt đầu</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Trung cấp</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">Cao cấp</span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div className="mb-6">
                        <span className="filter_title block my-2.5 font-medium text-xl">
                            Đánh giá
                        </span>
                        <ul className="pl-[15px] space-y-4">
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                    </span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                    </span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                    </span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                    </span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                            <li>
                                <label className="checkBox relative select-none cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="checkbox"
                                    />
                                    <span className="ml-6">
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35]"></i>
                                    </span>
                                    <span className="checkMark">
                                        <i className={`fas fa-check`}></i>
                                    </span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="courses_block basis-5/6 md:basis-2/3 sm:basis-full">
                    {coursesBySearch && (
                        <div>
                            <h3>Hiển thị {coursesBySearch.length} kết quả</h3>
                            <div>
                                {coursesBySearch.map((item, index) => {
                                    return (
                                        <ItemSearch
                                            course={item}
                                            key={index}
                                        ></ItemSearch>
                                    );
                                })}
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default SearchPage;
