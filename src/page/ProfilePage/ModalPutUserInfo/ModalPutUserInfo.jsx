import React from 'react';
import './ModalPutUserInfoStyle.css';
import { Form, Modal, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { hideModal } from '../../../app/ModalUserInfoReducer';
import { usersServ } from '../../../services/usersServ';
import { onLoading } from '../../../app/LoadingReducer';

const ModalPutUserInfo = ({ userInfo, setRerenderFlag, rerenderFlag }) => {
    const { isOpen } = useSelector((state) => state.open);
    const dispatch = useDispatch();

    const handleCancel = () => {
        dispatch(hideModal());
    };

    const onFinish = (values) => {
        const newUserInfo = {
            ...userInfo,
            hoTen: values.hoTen,
            matKhau: values.matKhau,
            email: values.email,
            soDT: values.soDT,
        };
        usersServ
            .putInfoUser(newUserInfo)
            .then((res) => {
                handleCancel();
                setRerenderFlag(!rerenderFlag);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const initialValues = {
        hoTen: userInfo.hoTen,
        matKhau: userInfo.matKhau,
        email: userInfo.email,
        soDT: userInfo.soDT,
    };

    const phoneValidation = (rule, value) => {
        const phoneRegex = /^[0-9]{10}$/; // Regex cho 10 chữ số

        if (value && !phoneRegex.test(value)) {
            return Promise.reject('Số điện thoại không hợp lệ');
        }

        return Promise.resolve();
    };

    return (
        <Modal
            open={isOpen}
            title="Chỉnh sửa thông tin cá nhân"
            onCancel={handleCancel}
            style={{ color: 'white', fontFamily: "'Roboto', sans-serif" }}
            footer={null}
        >
            <Form
                name="basic"
                style={{ maxWidth: 600 }}
                layout="vertical"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                initialValues={initialValues}
            >
                <Form.Item
                    label="Họ và tên"
                    name="hoTen"
                    rules={[
                        {
                            required: true,
                            message: 'Tên không được để trống!',
                        },
                    ]}
                >
                    <Input placeholder="Họ tên" />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="matKhau"
                    rules={[
                        {
                            required: true,
                            message: 'Mật khẩu không được để trống!',
                        },
                    ]}
                >
                    <Input.Password placeholder="Mật khẩu" />
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Email không được để trống!',
                        },
                        {
                            type: 'email',
                            message: 'Email không hợp lệ',
                        },
                    ]}
                >
                    <Input placeholder="Email" />
                </Form.Item>

                <Form.Item
                    label="Số điện thoại"
                    name="soDT"
                    rules={[
                        {
                            required: true,
                            message: 'Số điện thoại không được để trống!',
                        },
                        {
                            validator: phoneValidation,
                        },
                    ]}
                >
                    <Input placeholder="Số điện thoại" />
                </Form.Item>

                <Form.Item style={{ marginLeft: 'auto' }}>
                    <div className="mt-6 mr-4 text-right space-x-2">
                        <button
                            type="submit"
                            className="p-2.5 text-white bg-[#41b294] text-base"
                        >
                            Hoàn thành
                        </button>
                        <button
                            className="p-2.5 text-white bg-[#dc3545] text-base"
                            onClick={handleCancel}
                        >
                            Đóng
                        </button>
                    </div>
                </Form.Item>
            </Form>
        </Modal>
    );
};

export default ModalPutUserInfo;
