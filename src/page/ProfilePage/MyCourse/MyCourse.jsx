import React from 'react';
import ItemSearch from '../../SearchPage/ItemSearch/ItemSearch';
import { useState } from 'react';

const MyCourse = ({ courses, handleRemoveCourse }) => {
    const [coursesSearch, setCoursesSearch] = useState('');

    return (
        <div className="my-5">
            <div className="flex justify-between items-center">
                <h3 className="text-[17px] font-medium">KHÓA HỌC CỦA TÔI</h3>
                <form action="" onSubmit={(event) => event.preventDefault()}>
                    <input
                        type="text"
                        placeholder="Tìm kiếm"
                        className="search_input"
                        onChange={(event) => {
                            setCoursesSearch(event.target.value);
                        }}
                    />
                </form>
            </div>
            <div>
                {courses
                    ?.filter((item) =>
                        item.tenKhoaHoc
                            .toLowerCase()
                            .includes(coursesSearch.toLowerCase())
                    )
                    .map((item, index) => {
                        return (
                            <ItemSearch
                                course={item}
                                key={index}
                                handleRemoveCourse={handleRemoveCourse}
                            ></ItemSearch>
                        );
                    })}
            </div>
        </div>
    );
};

export default MyCourse;
