import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { usersServ } from '../../services/usersServ';
import './ProfilePageStyle.css';
import BannerGlobal from '../../components/Banner/BannerGlobal';
import avarUser from '../../assets/img/instrutor2.jpg';
import PersonalInfo from './PersonalInfo/PersonalInfo';
import MyCourse from './MyCourse/MyCourse';
import ModalPutUserInfo from './ModalPutUserInfo/ModalPutUserInfo';
import { useDispatch } from 'react-redux';
import { offLoading, onLoading } from '../../app/LoadingReducer';

const ProfilePage = () => {
    const [userInfo, setUserInfo] = useState();
    const [rerenderFlag, setRerenderFlag] = useState(false);
    let dispatch = useDispatch();

    useEffect(() => {
        dispatch(onLoading());
        usersServ
            .getInfoUser()
            .then((res) => {
                setUserInfo(res.data);
                dispatch(offLoading());
            })
            .catch((err) => {
                console.log(err);
                dispatch(offLoading());
            });
    }, [rerenderFlag]);

    const handleRemoveCourse = (maKhoaHoc) => {
        const data = {
            maKhoaHoc,
            taiKhoan: userInfo.taiKhoan,
        };

        usersServ.removeCourseEnrollment(data).then((res) => {
            setRerenderFlag(!rerenderFlag);
        });
    };

    const items = [
        {
            key: '1',
            label: `Thông tin cá nhân`,
            children: <PersonalInfo userInfo={userInfo}></PersonalInfo>,
        },
        {
            key: '2',
            label: `Khóa học`,
            children: (
                <MyCourse
                    courses={userInfo?.chiTietKhoaHocGhiDanh}
                    handleRemoveCourse={handleRemoveCourse}
                ></MyCourse>
            ),
        },
    ];

    return (
        userInfo && (
            <div className="mt-24 profilePage">
                <BannerGlobal
                    title="THÔNG TIN CÁ NHÂN"
                    subTitle="THÔNG TIN HỌC VIÊN"
                ></BannerGlobal>
                <div className="p-[50px]">
                    <div className="flex">
                        <div className="basis-1/4 md:basis-1/3 sm:hidden flex flex-col items-center">
                            <div className="pt-5">
                                <img
                                    src={avarUser}
                                    alt=""
                                    className="w-[150px] h-[150px] object-cover rounded-full"
                                />
                            </div>
                            <p className="font-medium mt-2.5">Robert Nguyễn</p>
                            <p className="my-2.5">Lập trình viên Front-end</p>
                            <button className="p-2.5 text-white bg-[#41b294] rounded-[20px]">
                                Hồ sơ cá nhân
                            </button>
                        </div>
                        <div className="basis-3/4 md:basis-2/3 sm:basis-full px-[15px] sm:px-0">
                            <Tabs
                                defaultActiveKey="1"
                                items={items}
                                type="card"
                                popupClassName="abc"
                                style={{ fontFamily: "'Roboto', sans-serif" }}
                            />

                            <ModalPutUserInfo
                                userInfo={userInfo}
                                setRerenderFlag={setRerenderFlag}
                                rerenderFlag={rerenderFlag}
                            ></ModalPutUserInfo>
                        </div>
                    </div>
                </div>
            </div>
        )
    );
};

export default ProfilePage;
