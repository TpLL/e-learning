import React from 'react';
import { useDispatch } from 'react-redux';
import { openModal } from '../../../app/ModalUserInfoReducer';

const PersonalInfo = ({ userInfo }) => {
    const dispath = useDispatch();

    return (
        <div className="personalInfo text-base">
            <div className="flex sm:flex-wrap">
                <div className="basis-7/12 sm:basis-full">
                    <div className="py-2.5">
                        <span className="font-medium mr-3">Email:</span>
                        <span className="text-[15px]">{userInfo.email}</span>
                    </div>
                    <div className="py-2.5">
                        <span className="font-medium mr-3">Họ và tên:</span>
                        <span className="text-[15px]">{userInfo.hoTen}</span>
                    </div>
                    <div className="py-2.5">
                        <span className="font-medium mr-3">Số điện thoại:</span>
                        <span className="text-[15px]">{userInfo.soDT}</span>
                    </div>
                </div>
                <div className="basis-5/12 sm:basis-full">
                    <div className="py-2.5">
                        <span className="font-medium mr-3">Tài khoản:</span>
                        <span className="text-[15px]">{userInfo.taiKhoan}</span>
                    </div>
                    <div className="py-2.5">
                        <span className="font-medium mr-3">Nhóm:</span>
                        <span className="text-[15px]">{userInfo.maNhom}</span>
                    </div>
                    <div className="py-2.5">
                        <span className="font-medium mr-3">Đối tượng:</span>
                        <span className="text-[15px]">
                            {userInfo.maLoaiNguoiDung === 'HV'
                                ? 'Học viên'
                                : 'Giáo vụ'}
                        </span>
                    </div>
                    <button
                        className="py-[5px] px-2.5 bg-[#f6ba35] text-white hover:scale-95 duration-300"
                        onClick={() => {
                            dispath(openModal());
                        }}
                    >
                        CẬP NHẬT
                    </button>
                </div>
            </div>

            <div className="my-[30px]">
                <h3 className="text-2xl font-medium">KĨ NĂNG CỦA TÔI</h3>
                <div className="flex md:flex-col gap-[30px]">
                    <div className="basis-8/12">
                        <div className="my-2.5 flex items-center space-x-1">
                            <button className="bg-[#f9ca9a] mr-[5px] text-white rounded-[8px] w-[60px] h-[45px]">
                                HTML
                            </button>
                            <div className="progress">
                                <div className="progress-bar progress-bar-striped progress-bar-animated progress-html"></div>
                            </div>
                        </div>

                        <div className="my-2.5 flex items-center space-x-1">
                            <button className="bg-[#f8bebb] mr-[5px] text-white rounded-[8px] w-[60px] h-[45px]">
                                CSS
                            </button>
                            <div className="progress">
                                <div className="progress-bar progress-bar-striped progress-bar-animated progress-css"></div>
                            </div>
                        </div>

                        <div className="my-2.5 flex items-center space-x-1">
                            <button className="bg-[#f0cc6b] mr-[5px] text-white rounded-[8px] w-[60px] h-[45px]">
                                JS
                            </button>
                            <div className="progress">
                                <div className="progress-bar progress-bar-striped progress-bar-animated progress-js"></div>
                            </div>
                        </div>

                        <div className="my-2.5 flex items-center space-x-1">
                            <button className="bg-[#113d3c] mr-[5px] text-white rounded-[8px] w-[60px] h-[45px]">
                                REACT
                            </button>
                            <div className="progress">
                                <div className="progress-bar progress-bar-striped progress-bar-animated progress-react"></div>
                            </div>
                        </div>
                    </div>
                    <div className="basis-4/12 text-white">
                        <div className="timeStudy grid grid-cols-2 gap-5">
                            <div className="col-span-1 p-[15px] bg-[#41b294] rounded-[20px]">
                                <div className="flex items-center gap-2 justify-center">
                                    <i className="fa-solid fa-user-clock sm:text-xs"></i>
                                    <div className="flex flex-col">
                                        <span className="font-medium">
                                            Giờ học
                                        </span>
                                        <span className="sm:text-xs">80</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-span-1 p-[15px] bg-[#41b294] rounded-[20px]">
                                <div className="flex items-center gap-2 justify-center">
                                    <i className="sm:text-xs fa-solid fa-layer-group"></i>
                                    <div className="flex flex-col">
                                        <span className="font-medium">
                                            Điểm tổng
                                        </span>
                                        <span className="sm:text-xs">80</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-span-1 p-[15px] bg-[#41b294] rounded-[20px]">
                                <div className="flex items-center gap-2 justify-center">
                                    <i className="sm:text-xs fa-solid fa-swatchbook"></i>
                                    <div className="flex flex-col">
                                        <span className="font-medium">
                                            Buổi học
                                        </span>
                                        <span className="sm:text-xs">40</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-span-1 p-[15px] bg-[#41b294] rounded-[20px]">
                                <div className="flex items-center gap-2 justify-center">
                                    <i className="sm:text-xs fa-solid fa-signal"></i>
                                    <div className="flex flex-col">
                                        <span className="font-medium">
                                            Cấp độ
                                        </span>
                                        <span className="sm:text-xs">
                                            Trung cấp
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-span-1 p-[15px] bg-[#41b294] rounded-[20px]">
                                <div className="flex items-center gap-2 justify-center">
                                    <i className="sm:text-xs fa-solid fa-graduation-cap"></i>
                                    <div className="flex flex-col">
                                        <span className="font-medium">
                                            Học lực
                                        </span>
                                        <span className="sm:text-xs">Khá</span>
                                    </div>
                                </div>
                            </div>

                            <div className="col-span-1 p-[15px] bg-[#41b294] rounded-[20px]">
                                <div className="flex items-center gap-2 justify-center">
                                    <i className="sm:text-xs fa-solid fa-book"></i>
                                    <div className="flex flex-col">
                                        <span className="font-medium">
                                            Bài tập
                                        </span>
                                        <span className="sm:text-xs">100</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PersonalInfo;
