import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopError from "./DesktopError";
import TableError from "./TableError";
import MobileError from "./MobileError";

export default function PageError() {
  return (
    <div>
      <Desktop>
        <DesktopError />
      </Desktop>
      <Tablet>
        <TableError />
      </Tablet>
      <Mobile>
        <MobileError />
      </Mobile>
    </div>
  );
}
