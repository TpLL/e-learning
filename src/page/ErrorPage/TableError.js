import React from 'react';
import Lottie from 'lottie-react';
import bg_lottie from '../../assets/lottie/lottie.json';

export default function TableError() {
    return (
        <div className="flex flex-col justify-center items-center w-screen h-screen">
            <div className="absolute z-10 top-24">
                <h1 className="text-8xl font-bold">404</h1>
            </div>
            <div className="relative">
                <Lottie
                    style={{ height: 500 }}
                    animationData={bg_lottie}
                    loop={true}
                />
            </div>
            <div className="font-mono	">
                <p className="text-4xl">Có gì đó sai ở đây</p>
            </div>
            <div className="">
                <button className="border px-2 py-3 bg-blue-500 font-bold text-white hover:scale-95 transition">
                    <a href="/" className="px-3 py-3 hover:no-underline	">
                        QUAY VỀ TRANG CHỦ
                    </a>
                </button>
            </div>
        </div>
    );
}
