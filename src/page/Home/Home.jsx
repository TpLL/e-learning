import React from 'react';
import Banner from '../../components/BannerHomePage/Banner';
import CourseInfo from '../../components/CourseInfo/CourseInfo';
import ListHome from '../../components/Course/ListHome';
import Quantity from '../../components/Quantity/Quantity';
import Instructor from '../../components/Instructor/Instructor';
import Review from '../../components/ReviewStudent/Review';

const Home = () => {
    return (
        <>
            <Banner></Banner>
            <CourseInfo></CourseInfo>
            <ListHome></ListHome>
            <Quantity></Quantity>
            <Instructor></Instructor>
            <Review></Review>
        </>
    );
};

export default Home;
