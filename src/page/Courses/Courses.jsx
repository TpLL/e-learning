import React, { useEffect, useState } from 'react';
import './CoursesStyle.css';
import { courseServ } from '../../services/courseServ';
import Item from '../../components/Course/Item';
import ReactPaginate from 'react-paginate';
import BannerGlobal from '../../components/Banner/BannerGlobal';
import { useDispatch } from 'react-redux';
import { offLoading, onLoading } from '../../app/LoadingReducer';

function Items({ currentItems }) {
    return (
        <div className="grid grid-cols-4 gap-8 md:grid-cols-2 sm:grid-cols-1">
            {currentItems &&
                currentItems.map((item, index) => (
                    <div key={index}>
                        <Item course={item}></Item>
                    </div>
                ))}
        </div>
    );
}

const Courses = () => {
    const [courseList, setCourseList] = useState([]);
    const [itemOffset, setItemOffset] = useState(0);
    const itemsPerPage = 12;
    const endOffset = itemOffset + itemsPerPage;
    const currentItems = courseList.slice(itemOffset, endOffset);
    const pageCount = Math.ceil(courseList.length / itemsPerPage);
    let dispatch = useDispatch();

    useEffect(() => {
        dispatch(onLoading());
        courseServ
            .getCoursesList()
            .then((res) => {
                setCourseList(res.data);
                dispatch(offLoading());
            })
            .catch((err) => {
                console.log(err);
                dispatch(offLoading());
            });
    }, []);

    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % courseList.length;
        console.log(
            `User requested page number ${event.selected}, which is offset ${newOffset}`
        );
        setItemOffset(newOffset);
    };

    return (
        <div className="mt-24">
            <BannerGlobal
                title="KHÓA HỌC"
                subTitle="BẮT ĐẦU HÀNH TRÌNH NÀO!!!"
            ></BannerGlobal>
            <div className="courses_banner p-[50px] text-white">
                <div className="flex bg-slate-800 sm:flex-wrap">
                    <div className="classBoxItem sm:basis-1/3 xs:basis-full text-center bg-[#264653]">
                        <p>CHƯƠNG TRÌNH HỌC</p>
                        <i className="fa-solid fa-laptop"></i>
                        <div>300</div>
                    </div>
                    <div className="classBoxItem sm:basis-1/3 xs:basis-full text-center bg-[#2a9d8f]">
                        <p>NHÀ SÁNG TẠO</p>
                        <i className="fa-solid fa-camera"></i>
                        <div>10000</div>
                    </div>
                    <div className="classBoxItem sm:basis-1/3 xs:basis-full text-center bg-[#e9c46a]">
                        <p>NHÀ THIẾT KẾ</p>
                        <i className="fa-solid fa-briefcase"></i>
                        <div>400</div>
                    </div>
                    <div className="classBoxItem sm:basis-1/3 xs:basis-full text-center bg-[#f4a261]">
                        <p>BÀI GIẢNG</p>
                        <i className="fa-solid fa-book"></i>
                        <div>3000</div>
                    </div>
                    <div className="classBoxItem sm:basis-1/3 xs:basis-full text-center bg-[#ee8959]">
                        <p>VIDEO</p>
                        <i className="fa-solid fa-circle-play"></i>
                        <div>40000</div>
                    </div>
                    <div className="classBoxItem sm:basis-1/3 xs:basis-full text-center bg-[#e76f51]">
                        <p>LĨNH VỰC</p>
                        <i className="fa-solid fa-dice-d20"></i>
                        <div>200</div>
                    </div>
                </div>
            </div>
            <div className="px-[50px]">
                <h2 className="mb-5">
                    <i className="fa-solid fa-bookmark text-[#ed85ab] text-xl mr-2"></i>
                    <span className="text-xl font-medium">
                        Danh sách khóa học
                    </span>
                </h2>
                <div>
                    <>
                        <Items currentItems={currentItems} />
                        <ReactPaginate
                            breakLabel="..."
                            nextLabel="Sau >"
                            onPageChange={handlePageClick}
                            pageRangeDisplayed={5}
                            pageCount={pageCount}
                            previousLabel="< Trước"
                            renderOnZeroPageCount={null}
                            containerClassName="pagination"
                            pageLinkClassName="page-num"
                            previousLinkClassName="page-num"
                            nextLinkClassName="page-num"
                            activeLinkClassName="active"
                        />
                    </>
                </div>
            </div>
        </div>
    );
};

export default Courses;
