import './DetailCourseStyle.css';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { courseServ } from '../../services/courseServ';
import instructor from '../../assets/img/instrutor.jpg';
import { useDispatch, useSelector } from 'react-redux';
import ItemHome from '../../components/Course/ItemHome';
import BannerGlobal from '../../components/Banner/BannerGlobal';
import { usersServ } from '../../services/usersServ';
import Swal from 'sweetalert2';
import { userLocal } from '../../services/QuanLyAPI/localUser';
import { offLoading, onLoading } from '../../app/LoadingReducer';
import imgBackup from '../../assets/img/imgBackup.jpg';

const DetailCourse = () => {
    let param = useParams();
    let dispatch = useDispatch();
    const [detailCourse, setDetailCourse] = useState();
    const [userInfo, setUserInfo] = useState();
    const { courses } = useSelector((state) => state.course);
    const [flagRerender, setFlagRerender] = useState(false);

    useEffect(() => {
        dispatch(onLoading());
        setUserInfo(userLocal.get());

        courseServ
            .getDetailCourse(param.id)
            .then((res) => {
                setDetailCourse(res.data);
                dispatch(offLoading());
            })
            .catch((err) => {
                console.log(err);
                dispatch(offLoading());
            });
    }, []);

    const handleRegister = () => {
        const dataRegister = {
            maKhoaHoc: detailCourse?.maKhoaHoc,
            taiKhoan: userInfo?.taiKhoan,
        };
        //kiểm tra login hay chưa
        //nếu login rồi
        if (userLocal.get()) {
            usersServ
                .postRegisterCourse(dataRegister)
                .then((res) => {
                    Swal.fire({
                        showConfirmButton: false,
                        icon: 'success',
                        title: 'Đăng ký thành công!',
                        timer: 1500,
                    });
                })
                .catch((err) => {
                    console.log(err);
                    Swal.fire({
                        showConfirmButton: false,
                        title: 'Đã đăng ký khóa học này rồi!',
                        icon: 'info',
                        text: 'Đã xảy ra lỗi vui lòng quay lại trang chủ hoặc thử lại',
                        timer: 1500,
                    });
                });
        } else {
            window.location.href = '/login';
        }
    };

    return (
        detailCourse && (
            <div className="mt-24 mb-8">
                <BannerGlobal
                    title="THÔNG TIN KHÓA HỌC"
                    subTitle="TIẾN LÊN VÀ KHÔNG CHẦN CHỪ!!!"
                ></BannerGlobal>

                <div className="course_detail flex sm:flex-col-reverse p-[50px]">
                    <div className="courseDetail sm:mt-5 pr-[15px] basis-2/3 md:basis-7/12">
                        <h2 className="font-medium text-2xl">
                            {detailCourse.tenKhoaHoc}
                        </h2>
                        <div className="flex md:flex-col my-[30px] sm:my-2">
                            <div className="flex flex-1 space-x-2 items-center p-[5px]">
                                <img
                                    src={instructor}
                                    alt=""
                                    className="w-[50px] h-[50px] rounded-full object-cover"
                                />
                                <div>
                                    <p className="text-sm text-[#8c8c8c]">
                                        Giảng viên
                                    </p>
                                    <p>Robert Ngô Ngọc</p>
                                </div>
                            </div>

                            <div className="flex flex-1 space-x-2 items-center p-[5px]">
                                <i className="fa-solid fa-graduation-cap text-[35px] text-[#41b294]"></i>
                                <div>
                                    <p className="text-sm text-[#8c8c8c]">
                                        Lĩnh vực
                                    </p>
                                    <p>
                                        {
                                            detailCourse.danhMucKhoaHoc
                                                .tenDanhMucKhoaHoc
                                        }
                                    </p>
                                </div>
                            </div>

                            <div className="flex-1 p-[5px]">
                                <div className="w-fit">
                                    <div className="flex items-center">
                                        <i className="fa-solid fa-star text-[#f6ba35] text-xl"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35] text-xl"></i>
                                        <i className="fa-solid fa-star text-[#f6ba35] text-xl"></i>
                                        <i className="fa-solid fa-star-half-stroke text-[#f6ba35] text-xl"></i>
                                        <i className="fa-regular fa-star text-[#f6ba35] text-xl"></i>
                                        <span className="font-semibold">
                                            3.5
                                        </span>
                                    </div>
                                    <div className="text-right">
                                        <span className="text-sm text-[#8c8c8c]">
                                            100 đánh giá
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p className="text-[#8c8c8c] sm:text-xs pb-5 border-b border-[#d0d0d0]">
                            React.js là thư viện JavaScript phổ biến nhất mà bạn
                            có thể sử dụng và tìm hiểu ngày nay để xây dựng giao
                            diện người dùng hiện đại, phản ứng cho web.Khóa học
                            này dạy bạn về React chuyên sâu, từ cơ bản, từng
                            bước đi sâu vào tất cả các kiến ​​thức cơ bản cốt
                            lõi, khám phá rất nhiều ví dụ và cũng giới thiệu cho
                            bạn các khái niệm nâng cao.Bạn sẽ nhận được tất cả
                            lý thuyết, hàng tấn ví dụ và bản trình diễn, bài tập
                            và bài tập cũng như vô số kiến ​​thức quan trọng bị
                            hầu hết các nguồn khác bỏ qua - sau cùng, có một lý
                            do tại sao khóa học này lại rất lớn! Và trong trường
                            hợp bạn thậm chí không biết tại sao bạn lại muốn học
                            React và bạn chỉ ở đây vì một số quảng cáo hoặc
                            "thuật toán" - đừng lo lắng: ReactJS là một công
                            nghệ quan trọng với tư cách là một nhà phát triển
                            web và trong khóa học này, tôi sẽ cũng giải thích
                            TẠI SAO điều đó lại quan trọng!
                        </p>

                        <div className="CourseLearn py-5">
                            <h3 className="py-2.5 text-xl font-medium">
                                Những gì bạn sẽ học
                            </h3>
                            <div className="flex sm:text-xs">
                                <div className="flex-1 px-[15px]">
                                    <ul>
                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Xây dựng các ứng dụng web mạnh
                                                mẽ, nhanh chóng, thân thiện với
                                                người dùng và phản ứng nhanh
                                            </span>
                                        </li>

                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Đăng ký công việc được trả lương
                                                cao hoặc làm freelancer trong
                                                một trong những lĩnh vực được
                                                yêu cầu nhiều nhất mà bạn có thể
                                                tìm thấy trong web dev ngay bây
                                                giờ
                                            </span>
                                        </li>

                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Cung cấp trải nghiệm người dùng
                                                tuyệt vời bằng cách tận dụng sức
                                                mạnh của JavaScript một cách dễ
                                                dàng
                                            </span>
                                        </li>

                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Tìm hiểu tất cả về React Hooks
                                                và React Components
                                            </span>
                                        </li>
                                    </ul>
                                </div>

                                <div className="flex-1 px-[15px]">
                                    <ul>
                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Thông thạo chuỗi công cụ hỗ trợ
                                                React, bao gồm cú pháp
                                                Javascript NPM, Webpack, Babel
                                                và ES6 / ES2015
                                            </span>
                                        </li>

                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Nhận ra sức mạnh của việc xây
                                                dựng các thành phần có thể kết
                                                hợp
                                            </span>
                                        </li>

                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Hãy là kỹ sư giải thích cách
                                                hoạt động của Redux cho mọi
                                                người, bởi vì bạn biết rất rõ
                                                các nguyên tắc cơ bản
                                            </span>
                                        </li>

                                        <li className="space-x-2 py-[5px]">
                                            <i className="fa-solid fa-check text-[#f6ba35]"></i>
                                            <span>
                                                Nắm vững các khái niệm cơ bản
                                                đằng sau việc cấu trúc các ứng
                                                dụng Redux
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div className="CourseContent">
                            <h3 className="py-2.5 text-xl font-semibold">
                                Nội dung khóa học
                            </h3>
                            <div className="py-5">
                                <div className="section_course bg-[#dcdcdc24] p-2.5 flex items-center">
                                    <span className="text-[22px] mr-5 sm:text-[18px]">
                                        MỤC 1: GIỚI THIỆU
                                    </span>
                                    <button className="p-[5px] border border-[#41b294] text-[#41b294] font-medium text-[15px] hover:bg-[#41b294] hover:text-white duration-500">
                                        XEM TRƯỚC
                                    </button>
                                </div>
                                <p className="mt-5 px-[5px]">Bài học</p>
                                <div className="lessonContainer text-[#8c8c8c] sm:text-xs">
                                    <ul>
                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Các khái niệm về React
                                                    Component
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Thiết lập môi trường cho
                                                    Windows
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Tạo ứng dụng React -
                                                    React-Scripts
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Ghi chú nhanh về dấu ngoặc
                                                    kép cho string interpolation
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="py-5">
                                <div className="section_course bg-[#dcdcdc24] p-2.5 flex items-center">
                                    <span className="text-[22px] mr-5 sm:text-[18px]">
                                        MỤC 2: KIẾN THỨC CĂN BẢN
                                    </span>
                                    <button className="p-[5px] border border-[#41b294] text-[#41b294] font-medium text-[15px] hover:bg-[#41b294] hover:text-white duration-500">
                                        XEM TRƯỚC
                                    </button>
                                </div>
                                <p className="mt-5 px-[5px]">Bài học</p>
                                <div className="lessonContainer text-[#8c8c8c] sm:text-xs">
                                    <ul>
                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Trang chủ và thành phần thư
                                                    mục
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Hướng dẫn khóa học + Liên
                                                    kết Github
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Trang chủ thương mại điện tử
                                                    + thiết lập SASS
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>Tệp CSS và SCSS</span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    React 17: Cập nhật các gói +
                                                    Phiên bản React mới nhất
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="py-5">
                                <div className="section_course bg-[#dcdcdc24] p-2.5 flex items-center">
                                    <span className="text-[22px] mr-5 sm:text-[18px]">
                                        MỤC 3: KIẾN THỨC CHUYÊN SÂU
                                    </span>
                                    <button className="p-[5px] border border-[#41b294] text-[#41b294] font-medium text-[15px] hover:bg-[#41b294] hover:text-white duration-500">
                                        XEM TRƯỚC
                                    </button>
                                </div>
                                <p className="mt-5 px-[5px]">Bài học</p>
                                <div className="lessonContainer text-[#8c8c8c] sm:text-xs">
                                    <ul>
                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    connect() and
                                                    mapStateToProps
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Trạng thái thư mục vào Redux
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>

                                        <li className="lessonItem">
                                            <div>
                                                <i className="fa-solid fa-circle-play mr-2.5 text-[#41b294]"></i>
                                                <span>
                                                    Thành phần Tổng quan về Bộ
                                                    sưu tập
                                                </span>
                                            </div>

                                            <div>
                                                <i className="fa-solid fa-clock mr-2.5 text-[#41b294]"></i>
                                                <span>14:35</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="sidebar pl-[15px] basis-1/3 md:basis-5/12 text-[#8c8c8c]">
                        <div className="sidebar_content py-2.5 px-[30px] md:px-2.5">
                            <img
                                src={detailCourse.hinhAnh}
                                onError={(e) => {
                                    e.target.src = imgBackup;
                                    setFlagRerender(!flagRerender);
                                }}
                                alt=""
                                className="w-full object-cover"
                            />
                            <div className="py-[30px] flex justify-end items-center">
                                <i className="fa-solid fa-bolt-lightning mr-2.5 text-[#f6ba35] text-[25px]"></i>
                                <div className="text-[25px] font-medium text-[#252525]">
                                    500.000
                                    <span className="align-top text-[19px]">
                                        đ
                                    </span>
                                </div>
                            </div>
                            <button
                                className="w-full p-[5px] border border-[#41b294] text-[#41b294] font-medium text-[15px] hover:bg-[#41b294] hover:text-white hover:scale-95 duration-500"
                                onClick={handleRegister}
                            >
                                ĐĂNG KÝ
                            </button>
                            <div className="sidebarDetail">
                                <div className="row py-5 flex justify-between items-center border-b border-[#f2f1f1]">
                                    <div>
                                        <span>Ghi danh: </span>
                                        <span className="text-[#252525] font-medium">
                                            10 học viên
                                        </span>
                                    </div>
                                    <i className="fa-solid fa-user-graduate text-[#f6ba35] text-xl"></i>
                                </div>
                                <div className="row py-5 flex justify-between items-center border-b border-[#f2f1f1]">
                                    <div>
                                        <span>Thời gian </span>
                                        <span className="text-[#252525] font-medium">
                                            18 giờ
                                        </span>
                                    </div>
                                    <i className="fa-regular fa-clock text-[#f6ba35] text-xl"></i>
                                </div>

                                <div className="row py-5 flex justify-between items-center border-b border-[#f2f1f1]">
                                    <div>
                                        <span>Bài học: </span>
                                        <span className="text-[#252525] font-medium">
                                            10
                                        </span>
                                    </div>
                                    <i className="fa-solid fa-book text-[#f6ba35] text-xl"></i>
                                </div>

                                <div className="row py-5 flex justify-between items-center border-b border-[#f2f1f1]">
                                    <div>
                                        <span>Video: </span>
                                        <span className="text-[#252525] font-medium">
                                            14
                                        </span>
                                    </div>
                                    <i className="fa-solid fa-photo-film text-[#f6ba35] text-xl"></i>
                                </div>

                                <div className="row py-5 flex justify-between items-center border-b border-[#f2f1f1]">
                                    <div>
                                        <span>Trình độ: </span>
                                        <span className="text-[#252525] font-medium">
                                            Người mới bắt đầu
                                        </span>
                                    </div>
                                    <i className="fa-solid fa-database text-[#f6ba35] text-xl"></i>
                                </div>
                            </div>
                            <form action="" className="formCoupon my-5">
                                <input
                                    type="text"
                                    placeholder="Nhập mã"
                                    className="inputCoupon w-full py-[3px] px-2.5 border border-[#d0d0d0]"
                                />
                            </form>
                        </div>
                    </div>
                </div>

                <div className="CoursesRct px-[50px]">
                    <h3 className="font-medium mb-7">Khóa học tham khảo</h3>
                    <div className="grid grid-cols-4 md:grid-cols-2 sm:grid-cols-1 gap-5">
                        {courses.payload &&
                            courses.payload.slice(7, 11).map((item, index) => {
                                return (
                                    <ItemHome
                                        course={item}
                                        key={index}
                                    ></ItemHome>
                                );
                            })}
                    </div>
                </div>
            </div>
        )
    );
};

export default DetailCourse;
