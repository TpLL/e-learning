import React from 'react';
import './blog.css';

export default function TableBlogPage() {
    return (
        <div>
            <div className="titleCarousel py-5">
                <h3 className="text-2xl font-bold">Blog</h3>
                <p className="text-xs">Thông tin công nghệ số!!!</p>
            </div>

            <div className="blogCourseContainer p-3">
                <h6 className="py-3">
                    <a href="#!" className="font-bold">
                        <i className="fa fa-bullhorn icontitle"></i>PHÙ HỢP VỚI
                        BẠN
                    </a>
                </h6>
                <div className="flex">
                    <div className="w-2/3 grid grid-cols-2 gap-3">
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://nordiccoder.com/app/uploads/2020/02/40.1.jpg"
                                ></img>
                            </div>
                            <h6>Thời gian và động lực</h6>
                            <div className="timeBlogCourse">
                                <div className="flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ
                                được gọi là "timetable". Hay dân dã hơn thì
                                người ta hay gọi là "Lịch thường nhật",...
                            </p>
                            <div className=" flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://vnskills.edu.vn/wp-content/uploads/2022/04/khoa-hoc-thiet-ke-lap-trinh-web-fullstack.jpg"
                                ></img>
                            </div>
                            <h6>Tailwind css và cách cài đặt cơ bản </h6>
                            <div className="timeBlogCourse">
                                <div className=" flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ
                                được gọi là "timetable". Hay dân dã hơn thì
                                người ta hay gọi là "Lịch thường nhật",...
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://braniumacademy.net/wp-content/uploads/2021/11/C-avatar.webp"
                                ></img>
                            </div>
                            <h6>Thời gian và động lực</h6>
                            <div className="timeBlogCourse ">
                                <div className="flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ
                                được gọi là "timetable". Hay dân dã hơn thì
                                người ta hay gọi là "Lịch thường nhật",...
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://zendvn.com/images/coursesOneToOne/pZoZpDJqIR.jpeg"
                                ></img>
                            </div>
                            <h6>Cấu trúc cơ bản trong HTML</h6>
                            <div className="timeBlogCourse">
                                <div className="flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Có lẽ cũng rất lâu rồi mà tôi chưa đụng đến thứ
                                được gọi là "timetable". Hay dân dã hơn thì
                                người ta hay gọi là "Lịch thường nhật",...
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://lh4.ggpht.com/-A6VYfOJUHVc/Xv05Qkh-E4I/AAAAAAAARFY/cyft9MsuRuU061woFix_4WH5BmugnhZPwCLcBGAsYHQ/s1600/Banner%2Bkhuye%25CC%2582%25CC%2581n%2Bma%25CC%2583i%2B72020%2B-%2B2.jpg"
                                ></img>
                            </div>
                            <h6>Material UI custom theme với TypeScript</h6>
                            <div className="timeBlogCourse">
                                <div className=" flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Tạo một component nhiều lúc cũng khá mất nhiều
                                thời gian nên mình xin giới thiệu extention này
                                cho mọi người nhé...
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://thantrieu.com/wp-content/uploads/2021/08/Banner_C_size43.png"
                                ></img>
                            </div>
                            <h6>Material UI custom theme với TypeScript</h6>
                            <div className="timeBlogCourse">
                                <div className=" flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Như các bạn đã biết chúng ta sẽ sử dụng
                                target="_blank" cho thẻ a để khi người dùng
                                click vô sẽ mở liên kết trên một tab mới...
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://timkhoahoc.com/wp-content/uploads/2021/01/KHOA-HOC-lap-trinh-web.jpg"
                                ></img>
                            </div>
                            <h6>Xử lý bất đồng bộ trong Javascript (phần 2)</h6>
                            <div className="timeBlogCourse">
                                <div className="flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Async/await là cơ chế giúp bạn thực thi các thao
                                tác bất đồng bộ một cách tuần tự hơn , giúp đoạn
                                code nhìn qua tưởng như đồng...
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                        <div className="itemBlog">
                            <div>
                                <img
                                    alt=""
                                    src="https://bkacad.edu.vn/images/course/2022/05/original/front-end_1652241413.png"
                                ></img>
                            </div>
                            <h6>
                                TyperScrip là gì, Vì sao nên dùng TyperScript
                            </h6>
                            <div className="timeBlogCourse">
                                <div className="flex justify-between">
                                    <span>
                                        <i className="fa fa-thumbs-o-up text-green-500"></i>
                                        300
                                    </span>
                                    <span>
                                        <i className="fa fa-comment-o text-green-500"></i>
                                        500
                                    </span>
                                    <span>
                                        <i className="fa fa-eye text-green-500"></i>
                                        200
                                    </span>
                                </div>
                            </div>
                            <p className="text-gray-400">
                                Link khóa học cho anh em nào tò mò ( Đừng lo vì
                                tất cả đều miễn......
                            </p>
                            <div className="flex justify-end">
                                <p>
                                    Đăng bởi
                                    <span className="text-pink-400">
                                        {' '}
                                        Jhony Đặng
                                    </span>
                                </p>
                            </div>
                            <div className="flex justify-center">
                                <button className="border px-3 py-2 bg-yellow-400 text-white hover:scale-95 transition">
                                    <a href="#!">XEM THÊM</a>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="w-1/3 pl-2 space-y-5">
                        <div className="itemBlog border">
                            <div className="blogRightBox">
                                <h6>Các chủ đề được đề xuất</h6>
                                <ul className="leading-4">
                                    <li>
                                        <a href="#!">Front-end / Mobile apps</a>
                                    </li>
                                    <li>
                                        <a href="#!">UI / UX / Design</a>
                                    </li>
                                    <li>
                                        <a href="#!">BACK-END</a>
                                    </li>
                                    <li>
                                        <a href="#!">Thư viện</a>
                                    </li>
                                    <li>
                                        <a href="#!">
                                            Chia sẻ người trong nghề
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#!">Châm ngôn IT</a>
                                    </li>
                                    <li>
                                        <a href="#!">Chủ đề khác</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="itemBlog border">
                            <div className="blogRightBox space-y-5">
                                <h6>Các chủ đề được đề xuất</h6>
                                <div className="px-3">
                                    <img
                                        alt=""
                                        src="https://topdev.vn/blog/wp-content/uploads/2021/03/tu-hoc-lap-trinh.jpg"
                                        style={{
                                            objectFit: 'cover',
                                            objectPosition: 'center',
                                        }}
                                    ></img>
                                    <h6 className="font-bold">
                                        Routing trong reactjs
                                    </h6>
                                    <p className="text-gray-400 my-1">
                                        Chúng ta sẽ cùng nhau tìm hiểu cách
                                        routing trong reactjs...
                                    </p>
                                    <div className="flex">
                                        <img
                                            alt=""
                                            style={{
                                                height: 40,
                                                width: 40,
                                                objectFit: 'cover',
                                                borderRadius: '50%',
                                            }}
                                            src="https://demo2.cybersoft.edu.vn/static/media/instrutor13.0159beae.jpg"
                                        ></img>
                                        <span className="text-gray-400 leading-10 ml-2">
                                            Nguyên Văn
                                        </span>
                                    </div>
                                </div>
                                <div className="px-3">
                                    <img
                                        alt=""
                                        src="https://res.cloudinary.com/dlmd9h8un/image/upload/c_fill,g_auto,w_448,h_270,dpr_2/f_webp,q_auto:good/v1642570462/Artboard-1-copy-6.png?_i=AA"
                                    ></img>
                                    <h6 className="font-bold">
                                        Lập trình hướng đối tượng oop
                                    </h6>
                                    <p className="text-gray-400">
                                        Chúng ta sẽ cùng nhau tìm hiểu cách oop
                                        trong reactjs...
                                    </p>
                                    <div className="flex ">
                                        <img
                                            alt=""
                                            style={{
                                                height: 40,
                                                width: 40,
                                                objectFit: 'cover',
                                                borderRadius: '50%',
                                            }}
                                            src="https://demo2.cybersoft.edu.vn/static/media/instrutor12.90a80820.jpg"
                                        ></img>
                                        <span className="text-gray-400 leading-10 ml-2">
                                            Nguyên Văn Vũ
                                        </span>
                                    </div>
                                </div>
                                <div className="px-3">
                                    <img
                                        alt=""
                                        src="https://danhgiakhoahoc.com/wp-content/uploads/2021/06/lap-trinh-web-2021-1024x576.jpg"
                                        style={{
                                            objectPosition: 'left',
                                            objectFit: 'cover',
                                        }}
                                    ></img>
                                    <h6 className="font-bold">
                                        Xử Lý Bất Đồng Bộ Trong Javascript
                                    </h6>
                                    <p className="text-gray-400 ">
                                        Chắc chắn khi lập trình, bạn sẽ có các
                                        công việc cần thời gian delay (gọi API,
                                        lấy dữ liệu từ Database, đọc/ghi
                                        file,...). Và đây...
                                    </p>
                                    <div className="flex ">
                                        <img
                                            alt=""
                                            style={{
                                                height: 40,
                                                width: 40,
                                                objectFit: 'cover',
                                                borderRadius: '50%',
                                            }}
                                            src="https://demo2.cybersoft.edu.vn/static/media/instrutor11.0387fe65.jpg"
                                        ></img>
                                        <span className="text-gray-400 leading-10 ml-2">
                                            Nguyên Minh
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
