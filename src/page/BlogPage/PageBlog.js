import React from "react";
import { Desktop, Mobile, Tablet } from "./responsive";
import DesktopBlogPage from "./DesktopBlogPage";
import TableBlogPage from "./TableBlogPage";
import MobileBlogPage from "./MobileBlogPage";

export default function PageBlog() {
  return (
    <div>
      <Desktop>
        <DesktopBlogPage />
      </Desktop>
      <Tablet>
        <TableBlogPage />
      </Tablet>
      <Mobile>
        <MobileBlogPage />
      </Mobile>
    </div>
  );
}
