import React from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './page/Home/Home';
import Layout from './layout/Layout';
import Courses from './page/Courses/Courses';
import DetailCourse from './page/DetailCourse/DetailCourse';
import CategoryCourses from './page/CategoryCourses/CategoryCourses';
import SearchPage from './page/SearchPage/SearchPage';
import ProfilePage from './page/ProfilePage/ProfilePage';
import PageThongTin from './page/thongTinPage/PageThongTin';
import PageSuKien from './page/suKienPage/PageSuKien';
import PageError from './page/ErrorPage/PageError';
import PageBlog from './page/BlogPage/PageBlog';
import PageAdQLKH from './page/AdminQLKH/PageAdQLKH';
import PageAdQLHV from './page/AdminQLHV/PageAdQLHV';
import PageLogin from './page/loginPage/PageLogin';
import Loading from './components/Loading/Loading.jsx';
import { userLocal } from './services/QuanLyAPI/localUser';

function App() {
    return (
        <div>
            <Loading></Loading>
            <BrowserRouter>
                <Routes>
                    <Route
                        path="/"
                        element={<Layout Component={Home}></Layout>}
                    ></Route>
                    <Route
                        path="/khoahoc"
                        element={<Layout Component={Courses}></Layout>}
                    ></Route>
                    <Route
                        path="/danhmuckhoahoc/:cate"
                        element={<Layout Component={CategoryCourses}></Layout>}
                    ></Route>
                    <Route
                        path="/chitiet/:id"
                        element={<Layout Component={DetailCourse}></Layout>}
                    ></Route>
                    <Route
                        path="/timkiem/:keyword"
                        element={<Layout Component={SearchPage}></Layout>}
                    ></Route>
                    <Route
                        path="/thongtincanhan"
                        element={<Layout Component={ProfilePage}></Layout>}
                    ></Route>

                    <Route path="/login" element={<PageLogin />} />
                    <Route
                        path="/thongtin"
                        element={<Layout Component={PageThongTin} />}
                    />
                    <Route
                        path="/sukien"
                        element={<Layout Component={PageSuKien} />}
                    />
                    <Route path="/sukien/lastyear" element={<PageError />} />
                    <Route path="/sukien/Noel" element={<PageError />} />
                    <Route path="*" element={<PageError />} />
                    <Route
                        path="/blog"
                        element={<Layout Component={PageBlog} />}
                    />
                    <Route
                        path="/admin/quanlynguoidung"
                        element={<PageAdQLHV />}
                    />
                    <Route
                        path='/admin/quanlykhoahoc'
                        element={<PageAdQLKH />}
                    />
                </Routes>
            </BrowserRouter>
        </div>
    );
}
export default App;
