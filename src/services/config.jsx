import { userLocal } from './QuanLyAPI/localUser';

export const BASE_URL = 'https://elearningnew.cybersoft.edu.vn';

export const configHeader = () => {
    return {
        TokenCybersoft:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNiIsIkhldEhhblN0cmluZyI6IjExLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDM5MDQwMDAwMCIsIm5iZiI6MTY3MDYwNTIwMCwiZXhwIjoxNjk0NTM4MDAwfQ.ndRbsF4IYaA6syfyNt9AVnZnm1kn9MAHUKKo4rXHLVE',
        Authorization: `Bearer ${userLocal.get()?.accessToken}`,
    };
};
