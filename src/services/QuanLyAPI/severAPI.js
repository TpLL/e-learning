import axios from "axios";
import { DATA_BASE, configHeaders, userHeaders } from "./constant";

class UserAPI {
  userLoginApi(userLogin) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: userLogin,
      headers: configHeaders(),
    });
  }
  userSignInApi(userSign) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: userSign,
      headers: configHeaders(),
    });
  }
  getListUserApi() {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01`,
      method: "GET",
      headers: configHeaders(),
    });
  }
  addUserApi(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/ThemNguoiDung`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
  findUser(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01&tuKhoa=${data}`,
      method: "GET",
      headers: configHeaders(),
    });
  }
  updateInforUser(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      method: "PUT",
      data: data,
      headers: userHeaders(),
    });
  }
  deleteUser(data) {
    return axios({
      url: `${DATA_BASE}//api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${data}`,
      method: "DELETE",
      headers: userHeaders(),
    });
  }
  getListCourseUser(data) {
    return axios({
      url: `${DATA_BASE}//api/QuanLyNguoiDung/LayDanhSachKhoaHocChuaGhiDanh?TaiKhoan=${data}`,
      method: "POST",
      headers: userHeaders(),
    });
  }
  getListCourseWait(taiKhoan) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`,
      method: "POST",
      data: taiKhoan,
      headers: userHeaders(),
    });
  }
  getListCourseDone(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
}

class CourseAPI {
  getListCourse() {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`,
      method: "GET",
      headers: configHeaders(),
    });
  }
  userRegisterCourse(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
  deleteCourseDone(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/HuyGhiDanh`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
  addCourse(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/ThemKhoaHoc`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
  getListCoursePathPage(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang?tenKhoaHoc=${data}`,
      method: "GET",
      headers: configHeaders(),
    });
  }
  fixCourse(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/CapNhatKhoaHoc`,
      method: "PUT",
      data: data,
      headers: configHeaders(),
    });
  }
  deleteCourse(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${data}`,
      method: "DELETE",
      headers: userHeaders(),
    });
  }
  getListUserWait(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
  getListUserDone(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
  getListUserUnregister(data) {
    return axios({
      url: `${DATA_BASE}/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh`,
      method: "POST",
      data: data,
      headers: userHeaders(),
    });
  }
}

export const courseAPI = new CourseAPI();
export const userAPI = new UserAPI();
