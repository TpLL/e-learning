import { userLocal } from "./localUser";

export const DATA_BASE = "https://elearningnew.cybersoft.edu.vn";
export const configHeaders = () => {
  return {
    TokenCybersoft:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNiIsIkhldEhhblN0cmluZyI6IjExLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDM5MDQwMDAwMCIsIm5iZiI6MTY3MDYwNTIwMCwiZXhwIjoxNjk0NTM4MDAwfQ.ndRbsF4IYaA6syfyNt9AVnZnm1kn9MAHUKKo4rXHLVE",
  };
};

export const userHeaders = () => {
  return {
    TokenCybersoft:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNiIsIkhldEhhblN0cmluZyI6IjExLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDM5MDQwMDAwMCIsIm5iZiI6MTY3MDYwNTIwMCwiZXhwIjoxNjk0NTM4MDAwfQ.ndRbsF4IYaA6syfyNt9AVnZnm1kn9MAHUKKo4rXHLVE",
    Authorization: "Bearer " + userLocal.get()?.accessToken,
  };
};
