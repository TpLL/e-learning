export const userLocal = {
  set: (dataString) => {
    let data = JSON.stringify(dataString);
    localStorage.setItem("DATA_LOGIN", data);
  },
  get: () => {
    let data = localStorage.getItem("DATA_LOGIN");
    return JSON.parse(data);
  },
  remove: () => {
    localStorage.removeItem("DATA_LOGIN");
  },
};

export const listUserLocal = {
  set: (dataString) => {
    let data = JSON.stringify(dataString);
    localStorage.setItem("LIST_USER", data);
  },
  get: () => {
    let data = localStorage.getItem("LIST_USER");
    return JSON.parse(data);
  },
  remove: () => {
    localStorage.removeItem("LIST_USER");
  },
};
