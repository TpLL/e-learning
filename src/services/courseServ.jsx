import axios from 'axios';
import { BASE_URL, configHeader } from './config';

export const courseServ = {
    getCoursesCate: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`,
            method: 'GET',
            headers: configHeader(),
        });
    },
    getCoursesList: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`,
            method: 'GET',
            headers: configHeader(),
        });
    },
    getDetailCourse: (id) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${id}`,
            method: 'GET',
            headers: configHeader(),
        });
    },
    getCoursesByCate: (cate) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${cate}&MaNhom=GP01`,
            method: 'GET',
            headers: configHeader(),
        });
    },
};
