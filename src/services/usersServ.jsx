import axios from 'axios';
import { BASE_URL, configHeader } from './config';

export const usersServ = {
    getInfoUser: () => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/ThongTinTaiKhoan`,
            method: 'POST',
            headers: configHeader(),
        });
    },
    putInfoUser: (userInfo) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
            method: 'PUT',
            data: userInfo,
            headers: configHeader(),
        });
    },
    postRegisterCourse: (data) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/DangKyKhoaHoc`,
            method: 'POST',
            data: data,
            headers: configHeader(),
        });
    },
    removeCourseEnrollment: (data) => {
        return axios({
            url: `${BASE_URL}/api/QuanLyKhoaHoc/HuyGhiDanh`,
            method: 'POST',
            data: data,
            headers: configHeader(),
        });
    },
};
