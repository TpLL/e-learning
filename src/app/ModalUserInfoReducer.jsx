import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isOpen: false,
};

export const ModalUserSlice = createSlice({
    name: 'open',
    initialState,
    reducers: {
        hideModal: (state) => {
            state.isOpen = false;
        },
        openModal: (state) => {
            state.isOpen = true;
        },
    },
});

export const { hideModal, openModal } = ModalUserSlice.actions;

export default ModalUserSlice.reducer;
