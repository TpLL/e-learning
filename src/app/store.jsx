import { combineReducers, configureStore } from '@reduxjs/toolkit';
import CourseReducer from './CourseReducer';
import ModalUserInfoReducer from './ModalUserInfoReducer';
import elearnningReducer from './elearningReducer';
import LoadingReducer from './LoadingReducer';

const reducer = combineReducers({
    course: CourseReducer,
    open: ModalUserInfoReducer,
    loading: LoadingReducer,
    elearnningReducer: elearnningReducer,
});

export const store = configureStore({
    reducer,
});
