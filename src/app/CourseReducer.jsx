import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    courses: [],
};

export const CourseSlice = createSlice({
    name: 'course',
    initialState,
    reducers: {
        addCourses: (state, action) => {
            state.courses = action;
        },
    },
});

export const { addCourses } = CourseSlice.actions;

export default CourseSlice.reducer;
