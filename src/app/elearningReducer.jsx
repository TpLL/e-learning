import { createSlice, current } from '@reduxjs/toolkit';
import { courseAPI, userAPI } from '../services/QuanLyAPI/severAPI';
import { message } from 'antd';
import { listUserLocal, userLocal } from '../services/QuanLyAPI/localUser';

const initialState = {
    valueUserLogin: userLocal.get(),
    listUser: listUserLocal.get(),
    registerCourse: [],
    listCourse: [],
};

const elearningReducer = createSlice({
    name: 'elearningReducer',
    initialState,
    reducers: {
        userLoginAction: (state, action) => {
            state.valueUserLogin = action.payload;
        },
        addUserAction: (state, action) => {
            state.listUser = action.payload;
        },
        fixUserAction: (state, action) => {
            let newListUser = [...current(state.listUser)];
            let index = newListUser.findIndex(
                (user) => user.taiKhoan === action.payload.taiKhoan
            );
            if (index !== -1) {
                newListUser[index] = action.payload;
            }
            state.listUser = newListUser;
        },
        deleteUserAction: (state, action) => {
            state.listUser = action.payload;
        },
        registerCourseAction: (state, action) => {
            state.registerCourse = action.payload;
        },
        deleteCourseDoneAction: (state, action) => {
            console.log(action.payload);
            state.registerCourse = action.payload;
        },
        addCourseAction: (state, action) => {
            console.log(action.payload);
            state.listCourse = action.payload;
        },
        fixCourseAction: (state, action) => {
            console.log(action.payload);
            state.listCourse = action.payload;
        },
        deleteCourseAction: (state, action) => {
            state.listCourse = action.payload;
        },
    },
});

export const {
    userLoginAction,
    addUserAction,
    fixUserAction,
    deleteUserAction,
    registerCourseAction,
    deleteCourseDoneAction,
    addCourseAction,
    fixCourseAction,
    deleteCourseAction,
} = elearningReducer.actions;

export default elearningReducer.reducer;

// action thunk------------------------------------------------------------------------------------------
export const userLogin = (values) => {
    console.log(values);
    return async (dispatch, getState) => {
        try {
            const result = await userAPI.userLoginApi(values);
            console.log(result.data);
            message.success('Đăng nhập thành công !');
            userLocal.set(result.data);
            const action = userLoginAction(result.data);
            dispatch(action);
            window.location.href = '/';
        } catch (err) {
            console.log(err);
            message.error('Đăng nhập thất bại !');
        }
    };
};

export const addUserThunk = (values) => {
    console.log(values);
    return async (dispatch, getState) => {
        try {
            const result = await userAPI
                .addUserApi(values)
                .then((result) => {
                    console.log(result.data);
                })
                .catch((err) => {
                    console.log(err);
                });
            const newListUser = await userAPI.getListUserApi();
            listUserLocal.set('LIST_USER', newListUser.data);
            dispatch(addUserAction(newListUser.data));
            message.success("Thêm người dùng thành công!")
        } catch (err) {
            console.log(err.response.data);
            alert(err.response.data);
        }
    };
};

export const fixUserThunk = (values) => {
    console.log(values);
    return async (dispatch, getState) => {
        try {
            const result = await userAPI.updateInforUser(values);
            message.success('Thay đổi thông tin tài khoản thành công!');
            const action = fixUserAction(result.data);
            dispatch(action);
        } catch (err) {
            console.log(err);
            message.error(err.response.data)
        }
    };
};

export const deleteUserThunk = (values) => {
    console.log(values);
    return async (dispatch, getState) => {
        try {
            const result = await userAPI.deleteUser(values.taiKhoan);
            const newListUser = await userAPI.getListUserApi();
            dispatch(deleteUserAction(newListUser.data));
            message.success("Xoá người dùng thành công!")
        } catch (err) {
            console.log(err);
        }
    };
};

export const registerCourseThunk = (values) => {
    return async (dispatch, getState) => {
        try {
            const result = await courseAPI.userRegisterCourse(values);
            message.success(result.data);
            const listCourseDone = await userAPI.getListCourseDone({
                taiKhoan: `${values.taiKhoan}`,
            });
            dispatch(registerCourseAction(listCourseDone.data));
        } catch (err) {
            console.log(err);
        }
    };
};

export const deleteCourseUserThunk = (values) => {
    console.log(values);
    return async (dispatch, getState) => {
        try {
            const result = await courseAPI.deleteCourseDone(values);
            message.success(result.data);
            const listCourseDone = await userAPI.getListCourseDone({
                taiKhoan: `${values.taiKhoan}`,
            });
            dispatch(deleteCourseDoneAction(listCourseDone.data));
        } catch (err) {
            console.log(err);
        }
    };
};

//  ///////////////// action thunk course
export const addCourseThunk = (values) => {
    let { hinhAnh } = values;
    values.hinhAnh = hinhAnh[0];
    let newValues = values;
    console.log(newValues);
    return async (dispatch, getState) => {
        try {
            const result = await courseAPI.addCourse(newValues);
            console.log(result.data);
            message.success('Tạo khoá học thành công!');
            const newListCourse = await courseAPI.getListCourse();
            console.log(newListCourse.data);
            dispatch(addCourseAction(newListCourse.data));
        } catch (err) {
            console.log(err);
            message.error(err.response.data);
        }
    };
};

export const fixCourseThunk = (values) => {
    // console.log(values);
    let { hinhAnh } = values;
    // console.log(hinhAnh[0]);
    values.hinhAnh = hinhAnh[0];
    return async (dispatch, getState) => {
        try {
            const result = await courseAPI.fixCourse(values);
            message.success('Cập nhật khoá học thành công!');
            const getListCourse = await courseAPI.getListCourse();
            dispatch(fixCourseAction(getListCourse.data));
        } catch (err) {
            console.log(err);
            message.error(err.response.data);
        }
    };
};

export const deleteCourseThunk = (values) => {
    console.log(values);
    return async (dispatch, getState) => {
        try {
            const result = await courseAPI.deleteCourse(values.maKhoaHoc);
            const newListCorse = await courseAPI.getListCourse();
            dispatch(deleteCourseAction(newListCorse.data));
        } catch (err) {
            message.error(err.response.data)
    };
}}
